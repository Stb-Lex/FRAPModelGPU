import sys
import subprocess
import time

sys.path.append("../validate/")
import common

def parallel_sssdb_gpu(timestamp, simulationFilename, outputFolder, outputFilename, logDirectory):

    # We first launch two instance of signal_stochastic_scan_db on the GPUs
    print("Launching two instances of signal_stochastic_scan_db on the GPUs...")
    stdout0 = open(logDirectory+"/"+timestamp+"_sssdb_gpu0.txt", 'w')
    time.sleep(2)
    stdout1 = open(logDirectory+"/"+timestamp+"_sssdb_gpu1.txt", 'w')
    
    proc0 = subprocess.Popen(["./signal_stochastic_scan_db", "../cfg/device_gpu0.cfg", simulationFilename, "temp_gpu_out0.csv"], stdout=stdout0, shell=False)
    proc1 = subprocess.Popen(["./signal_stochastic_scan_db", "../cfg/device_gpu1.cfg", simulationFilename, "temp_gpu_out1.csv"], stdout=stdout1, shell=False)
    
    print("Done. Waiting for termination.")

    proc0.wait()
    proc1.wait()

    stdout0.close()
    stdout1.close()
    
    print("Computation done. Summing outputs and saving on disk as ", outputFolder+"/"+outputFilename)
    
    # We collect their outputs, sum them, then save

    data0, dimxy0, frames0 = common.load_frames_from_file("temp_gpu_out0.csv")
    data1, dimxy1, frames1 = common.load_frames_from_file("temp_gpu_out1.csv")

    data_out = data0 + data1

    common.export_pickle(outputFolder+'/'+timestamp+outputFilename, data_out, dimxy0, frames0)

    
def parallel_sssdb_cpu(timestamp, simulationFilename, outputFolder, outputFilename, logDirectory):

    # We first launch two instance of signal_stochastic_scan_db on the CPUs
    print("Launching one instances of signal_stochastic_scan_db on the CPUs...")
    stdout0 = open(logDirectory+timestamp+"_sssdb_cpu0.txt", 'w')
    
    proc0 = subprocess.Popen(["./signal_stochastic_scan_db", "device_cpu.cfg", sys.argv[1], "temp_cpu_out0.csv"], stdout=stdout0, shell=False)
    
    print("Done. Waiting for termination.")

    proc0.wait()

    stdout0.close()
    
    print("Computation done. Summing outputs and saving on disk as ", outputFolder+"/"+outputFilename)
    
    # We collect their outputs, sum them, then save

    data0, dimxy0, frames0 = common.load_frames_from_file("temp_cpu_out0.csv")

    common.export_pickle(outputFolder+'/'+timestamp+outputFilename, data0, dimxy0, frames0)

