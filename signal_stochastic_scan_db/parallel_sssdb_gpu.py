import sys
import parallel_sssdb

if(len(sys.argv) == 1):
    print("Usage: python3 parallel_sssdb_gpu simulationFilename outputFilename")
    exit()

parallel_sssdb.parallel_sssdb_gpu("", sys.argv[1], "./", sys.argv[2], "./")
