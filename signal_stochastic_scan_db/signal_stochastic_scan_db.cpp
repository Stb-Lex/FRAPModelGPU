#include"signal_stochastic_scan_db.h"
#include<libconfig.h++>
#include<ctime>
#include<cmath>
#include<vector>

std::ostream& operator<<(std::ostream& stram, const simulation_sssdb_param_t& param)
{
  std::cout << "Simulation parameters:" << std::endl;
  std::cout << "Resolution N = " << param.N << ", padding = " << param.padding << std::endl;
  std::cout << "Effective resolution = [" << param.dimxy[0] << ", " << param.dimxy[1] << "]" << std::endl;
  std::cout << "Number of bleach frames = " << param.bleachFrames << ", number of frames = " << param.frames << std::endl;
  std::cout << "Number of workers = " << param.workers << ", number of particles per worker = " << param.particlesPerWorker << ", total numbers of particles = " << param.Nparticles << std::endl;
  if(param.foundOmega)
    std::cout << "Omega = " << param.omega << "Hz" << std::endl;
  std::cout << "PDT = " << param.PDT << ", NLT = " << param.NLT << ", BTOT = " << param.BTOT <<  ", simulation steps = " << param.simSteps << std::endl;
  std::cout << "Diffusion-binding simulation: " << param.db << std::endl;
  if(param.db)
  {
    std::cout << "Kon = " << param.k[0] << ", koff = " << param.k[1] << std::endl;
    std::cout << "muon = " << param.mu[0] << ", muoff = " << param.mu[1] << std::endl;
    std::cout << "pion = " << param.pi[0] << ", pioff = " << param.pi[1] << std::endl;
  }
  std::cout << "Diffusion constant D = " << param.D << std::endl;
  std::cout << "Bleach coefficient = " << param.bleachCoef << ", reading bleach coefficient = " << param.readBleachCoef << std::endl;
  std::cout << "Bleach spot shape: " << param.bleachType << std::endl;
  if(param.bleachType == "disk" || param.bleachType == "gaussian")
    std::cout << "Radius = " << param.bleachRadius << std::endl;
  else if(param.bleachType == "rect")
    std::cout << "Rectangle dimension = [" << param.bleachRect[0] << ", " << param.bleachRect[1] << "]" << std::endl;
  std::cout << "Bleach zoom factor = " << param.zoomFactor << std::endl;
  std::cout << "Seed = " << param.seed;
}

bool loadSimulationParamFromConfigFile(simulation_sssdb_param_t& param, const char* configFilename)
{
  libconfig::Config cfg;

  try
  {
    cfg.readFile(configFilename);
  }
  catch(libconfig::ParseException& e)
  {
    std::cout << "Error when parsing simulation config file " << configFilename << " at line " << e.getLine() << std::endl;
    std::cout << e.getError() << std::endl;
    return false;
  }
  catch(libconfig::FileIOException& e)
  {
    std::cout << "Could not open simulation config file " << configFilename << std::endl;
    return false;
  }

  try
  {
    cfg.lookup("simulation.N");
    cfg.lookup("simulation.padding");
    cfg.lookup("simulation.frames");
    cfg.lookup("simulation.bleachFrames");
    cfg.lookup("simulation.workers");
    cfg.lookup("simulation.particlesPerWorker");
    cfg.lookup("simulation.D");
    cfg.lookup("simulation.simSteps");
    cfg.lookup("simulation.BTOT");
    cfg.lookup("simulation.kon");
    cfg.lookup("simulation.koff");
    cfg.lookup("simulation.bleachType");
    cfg.lookup("simulation.bleachCoef");
    cfg.lookup("simulation.readBleachCoef");
    cfg.lookup("simulation.db");
    cfg.lookup("simulation.seeded");
    cfg.lookup("simulation.zoomFactor");
    cfg.lookup("simulation.pixelStride");

    std::string bleachType;
    cfg.lookupValue("simulation.bleachType", bleachType);
    if(bleachType == "disk" || bleachType == "gaussian")
      cfg.lookup("simulation.bleachRadius");
    else if(bleachType == "rect")
    {
      const libconfig::Setting& bleachRect = cfg.lookup("simulation.bleachRect");
      if(bleachRect.getLength() != 2)
      {
	std::cout << "Badly formed array for bleachRect!" << std::endl;
	return false;
      }
    }
    else if(bleachType == "none");
    else
    {
      std::cerr << "Could not recognise bleach type: " << bleachType << std::endl;
      return false;
    }
  }
  catch(libconfig::SettingNotFoundException& e)
  {
    std::cout << "Error when loading simulation parameters from config file! Could not find: " << e.getPath() << std::endl;
    return false;
  }

  param.foundOmega = true;
  try
  {
    cfg.lookup("simulation.omega");
  }
  catch(libconfig::SettingNotFoundException& e)
  {
    param.foundOmega = false;
  }

  if(!param.foundOmega)
  {
    try
    {
      cfg.lookup("simulation.PDT");
      cfg.lookup("simulation.NLT");
    }
    catch(libconfig::SettingNotFoundException& e)
    {
      std::cerr << "Error when loading simulation parameters from config file! Omega could not be found but PDT and NLT are not provided!" << std::endl;
      return false;
    }
  }
  
  cfg.lookupValue("simulation.N", param.N);
  cfg.lookupValue("simulation.padding", param.padding);
  param.dimxy[0] = param.N+2*param.padding;
  param.dimxy[1] = param.N+2*param.padding;
  cfg.lookupValue("simulation.frames", param.frames);
  cfg.lookupValue("simulation.bleachFrames", param.bleachFrames);
  cfg.lookupValue("simulation.workers", param.workers);
  cfg.lookupValue("simulation.particlesPerWorker", param.particlesPerWorker);
  param.Nparticles = param.workers*param.particlesPerWorker;
  cfg.lookupValue("simulation.bleachCoef", param.bleachCoef);
  cfg.lookupValue("simulation.readBleachCoef", param.readBleachCoef);
  cfg.lookupValue("simulation.simSteps", param.simSteps);
  cfg.lookupValue("simulation.BTOT", param.BTOT);
  cfg.lookupValue("simulation.kon", param.k[0]);
  cfg.lookupValue("simulation.koff", param.k[1]);
  param.mu[0] = 1.f/param.k[0];
  param.mu[1] = 1.f/param.k[1];
  param.pi[0] = param.k[1]/(param.k[0]+param.k[1]);
  param.pi[1] = param.k[0]/(param.k[0]+param.k[1]);
  cfg.lookupValue("simulation.D", param.D);
  cfg.lookupValue("simulation.db", param.db);
  cfg.lookupValue("simulation.seeded", param.seeded);
  cfg.lookupValue("simulation.zoomFactor", param.zoomFactor);
  cfg.lookupValue("simulation.pixelStride", param.pixelStride);

  if(param.foundOmega)
  {
    cfg.lookupValue("simulation.omega", param.omega);

    param.PDT = 0.295f/1.2f/param.omega/param.N;
    param.NLT = 0.7542f/param.omega;
  }
  else
  {
    cfg.lookupValue("simulation.NLT", param.NLT);
    cfg.lookupValue("simulation.PDT", param.PDT);
  }

  if(param.seeded)
  {
    if(!cfg.lookupValue("simulation.seed", param.seed))
    {
      std::cout << "Warning! Simulation seed should be provided! Generated instead." << std::endl;
      param.seed = std::time(nullptr);
    }
  }
  else
    param.seed = time(nullptr);

  cfg.lookupValue("simulation.bleachType", param.bleachType);
  if(param.bleachType == "disk" || param.bleachType == "gaussian")
  {
    cfg.lookupValue("simulation.bleachRadius", param.bleachRadius);
  }
  else if(param.bleachType == "rect")
  {
    const libconfig::Setting& bleachRect = cfg.lookup("simulation.bleachRect");
    param.bleachRect[0] = bleachRect[0];
    param.bleachRect[1] = bleachRect[1];
  }

  return true;
}

cl_int sssdb_bleach_one_frame(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufSeeds, cl_mem bufPos, cl_mem bufState, cl_mem bufBleachMask, size_t Nworkers, size_t particlesPerWorker, float D, float mu[2], float PDT, float BTOT, float NLT, unsigned int simsteps, unsigned int pixelStride, const size_t dimxy[2], float zoomFactor, bool isDB)
{
  cl_int clerror;
  cl_program bleach_program;
  cl_kernel bleach_kernel;

  ParameterList parameters;
  parameters.push_back(Parameter("SIMSTEPS", (int)simsteps));
  parameters.push_back(Parameter("PDT", PDT));
  parameters.push_back(Parameter("BTOT", BTOT));
  parameters.push_back(Parameter("NLT", NLT));
  parameters.push_back(Parameter("DIMX", (int)dimxy[0]));
  parameters.push_back(Parameter("DIMY", (int)dimxy[1]));
  parameters.push_back(Parameter("PARTICLESPERWORKER", (int)particlesPerWorker));
  parameters.push_back(Parameter("D", D));
  parameters.push_back(Parameter("MUON", mu[0]));
  parameters.push_back(Parameter("MUOFF", mu[1]));
  parameters.push_back(Parameter("ZOOM", zoomFactor));
  parameters.push_back(Parameter("PIXELSTRIDE", (int)pixelStride));
  if(isDB)
    parameters.push_back(Parameter("DB", 1));

  clerror = loadKernel("../common_cl/stochastic_scan_bleach.cl", "stochastic_scan_bleach", "-I ../common_cl/", context, deviceId, &bleach_program, &bleach_kernel, &parameters);

  if(clerror != CL_SUCCESS)
    return clerror;

  size_t workGroupSize = 128;
  
  OCLCheck(clSetKernelArg(bleach_kernel, 0, sizeof(cl_mem), &bufSeeds));
  OCLCheck(clSetKernelArg(bleach_kernel, 1, sizeof(cl_mem), &bufPos));
  OCLCheck(clSetKernelArg(bleach_kernel, 2, sizeof(cl_mem), &bufState));
  OCLCheck(clSetKernelArg(bleach_kernel, 3, sizeof(cl_mem), &bufBleachMask));
  OCLCheck(clSetKernelArg(bleach_kernel, 4, sizeof(cl_float2)*workGroupSize*particlesPerWorker, nullptr));
  OCLCheck(clSetKernelArg(bleach_kernel, 5, sizeof(cl_uchar)*workGroupSize*particlesPerWorker, nullptr));

  clerror = clEnqueueNDRangeKernel(commandQueue, bleach_kernel, 1, nullptr, &Nworkers, &workGroupSize, 0, nullptr, nullptr);

  if(clerror != CL_SUCCESS)
  {
    std::cerr << "Error during bleaching! OpenCL error code: " << clerror;
    return clerror;
  }

  OCLCheck(clFinish(commandQueue));

  OCLCheck(clReleaseKernel(bleach_kernel));
  OCLCheck(clReleaseProgram(bleach_program));

  return CL_SUCCESS;
}

cl_int sssdb_diffuse_one_frame(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufSeeds, cl_mem bufPos, cl_mem bufState, cl_mem bufBin, size_t Nworkers, size_t particlesPerWorker, float D, float mu[2], float bleachCoef, float PDT, float BTOT, float NLT, unsigned int simsteps, unsigned int pixelStride, const size_t dimxy[2], bool isDB)
{
  cl_int clerror;
  cl_program scan_program;
  cl_kernel scan_kernel;

  ParameterList parameters;
  parameters.push_back(Parameter("SIMSTEPS", (int)simsteps));
  parameters.push_back(Parameter("PDT", PDT));
  parameters.push_back(Parameter("BTOT", BTOT));
  parameters.push_back(Parameter("NLT", NLT));
  parameters.push_back(Parameter("DIMX", (int)dimxy[0]));
  parameters.push_back(Parameter("DIMY", (int)dimxy[1]));
  parameters.push_back(Parameter("BLEACHCOEF", bleachCoef));
  parameters.push_back(Parameter("PARTICLESPERWORKER", (int)particlesPerWorker));
  parameters.push_back(Parameter("D", D));
  parameters.push_back(Parameter("MUON", mu[0]));
  parameters.push_back(Parameter("MUOFF", mu[1]));
  parameters.push_back(Parameter("PIXELSTRIDE", (int)pixelStride));
  if(isDB)
    parameters.push_back(Parameter("DB", 1));
  
  clerror = loadKernel("../common_cl/stochastic_scan_diffuse.cl", "stochastic_scan_db", "-I ../common_cl/", context, deviceId, &scan_program, &scan_kernel, &parameters);

  if(clerror != CL_SUCCESS)
    return clerror;

  size_t workGroupSize = 128;
  
  OCLCheck(clSetKernelArg(scan_kernel, 0, sizeof(cl_mem), &bufSeeds));
  OCLCheck(clSetKernelArg(scan_kernel, 1, sizeof(cl_mem), &bufPos));
  OCLCheck(clSetKernelArg(scan_kernel, 2, sizeof(cl_mem), &bufState));
  OCLCheck(clSetKernelArg(scan_kernel, 3, sizeof(cl_mem), &bufBin));
  OCLCheck(clSetKernelArg(scan_kernel, 4, sizeof(cl_float2)*workGroupSize*particlesPerWorker, nullptr));
  OCLCheck(clSetKernelArg(scan_kernel, 5, sizeof(cl_uchar)*workGroupSize*particlesPerWorker, nullptr));

  clerror = clEnqueueNDRangeKernel(commandQueue, scan_kernel, 1, nullptr, &Nworkers, &workGroupSize, 0, nullptr, nullptr);

  if(clerror != CL_SUCCESS)
  {
    std::cerr << "Error during diffusion! OpenCL error code: " << clerror;
    return clerror;
  }

  OCLCheck(clFinish(commandQueue));

  OCLCheck(clReleaseKernel(scan_kernel));
  OCLCheck(clReleaseProgram(scan_program));

  return CL_SUCCESS;
}

