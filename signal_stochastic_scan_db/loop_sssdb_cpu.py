import parallel_sssdb
import time
import sys


if(len(sys.argv) != 5):
    print("Usage: python3 loop_sssdb_cpu.py simulationFile, outputDirectory, outputFilename, outputLogDirectory")
    exit()
    
while True:
    ts = time.strftime("%Y%m%d_%H%M%S", time.gmtime())

    parallel_sssdb.parallel_sssdb_cpu(ts, sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])

