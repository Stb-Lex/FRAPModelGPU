#ifndef SIGNAL_STOCHASTIC_SCAN_DB
#define SIGNAL_STOCHASTIC_SCAN_DB

#include"opencl_common.h"
#include<iostream>

struct simulation_sssdb_param_t
{
  unsigned int N;
  unsigned int padding;
  size_t dimxy[2];
  unsigned int frames;
  unsigned int bleachFrames;
  
  unsigned int workers;
  unsigned int particlesPerWorker;
  unsigned int Nparticles;

  float zoomFactor;
  float bleachCoef;
  float readBleachCoef;
  
  unsigned int simSteps;
  unsigned int pixelStride;
  bool foundOmega;
  float omega;
  float PDT;
  float BTOT;
  float NLT;

  bool db;
  float k[2];
  float mu[2];
  float pi[2];
  float D;

  std::string bleachType;
  float bleachRadius;
  float bleachRect[2];

  bool seeded;
  unsigned int seed;
};

std::ostream& operator<<(std::ostream& stream, const simulation_sssdb_param_t& param);

bool loadSimulationParamFromConfigFile(simulation_sssdb_param_t& param, const char* szConfigFilename);

cl_int sssdb_bleach_one_frame(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufSeeds, cl_mem bufPos, cl_mem bufState, cl_mem bufBleachMask, size_t Nworkers, size_t particlesPerWorker, float D, float mu[2], float PDT, float BTOT, float NLT, unsigned int simsteps, unsigned int pixelStride, const size_t dimxy[2], float zoomFactor, bool isDB = true);

cl_int sssdb_diffuse_one_frame(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufSeeds, cl_mem bufPos, cl_mem bufState, cl_mem bufBin, size_t Nworkers, size_t particlesPerWorker, float D, float mu[2], float bleachCoef, float PDT, float BTOT, float NLT, unsigned int simsteps, unsigned int pixelStride, const size_t dimxy[2], bool isDB = true);



#endif // SIGNAL_STOCHASTIC_SCAN_DB
