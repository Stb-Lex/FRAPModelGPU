import sys
import os
import glob

sys.path.append("../validate/")
import common

dataFiles = glob.glob(sys.argv[1] + "*.pickle")
dataFiles.extend(glob.glob(sys.argv[1] + "*.csv"))


print("Initial file", dataFiles[0])
data, dimxy, frames = common.load_frames_from_file(dataFiles[0])
data_out = data

for i in range(1, len(dataFiles)):
    print("Stacking", dataFiles[i])
    data, dimxy, frames = common.load_frames_from_file(dataFiles[i])

    data_out += data

print("Saving under", sys.argv[2])
common.export_pickle(sys.argv[2], data_out, dimxy, frames)
