#include<CL/opencl.h>
#include<iostream>
#include<string>
#include<Eigen/Dense>
#include<cmath>
#include<libconfig.h++>

#include"timer.h"
#include"opencl_common.h"
#include"signal_stochastic_db.h"
#include"mask.h"
#include"particles_common.h"
#include"initial_sdb.h"
#include"export_solution.h"
#include"signal_stochastic_scan_db.h"

#define DEBUG_BLEACH_FRAMES
//#define DEBUG_DUMP_PARTICLES

#define WITH_CONSOLIDATION

int main(int argc, char* argv[])
{
  putenv("CUDA_CACHE_DISABLE=1");

  if(argc != 4)
  {
    std::cout << "Usage: signal_stochastic_scan_db device_config_filename simulation_config_filename output_filename" << std::endl;
    return 0;
  }  

  const char* szDeviceConfigFilename = argv[1];
  const char* szSimulationConfigFilename = argv[2];
  std::string szOutputFilename = argv[3];
  
  // Physic vars
  const unsigned int seed = 1337;
  
  // OpenCL related vars  
  cl_platform_id platformId;
  cl_device_id deviceId;
  cl_context context;
  cl_command_queue commandQueue;
  cl_int clerror;

  Timer myPerfTimer;
  std::cout << "Initialising OpenCL from " << szDeviceConfigFilename << "... " << std::flush;

  if(initOpenCLByConfigFile(platformId, context, deviceId, commandQueue, argv[1]) != CL_SUCCESS)
    return -1;

  std::cout << "Done. Elapsed time: " << myPerfTimer.Elapsed() << "s" << std::endl;

  std::cout << "Loading simulation parameters from " << szSimulationConfigFilename << "... " << std::flush;
  simulation_sssdb_param_t param;
  if(!loadSimulationParamFromConfigFile(param, szSimulationConfigFilename))
    return -1;
  std::cout << "Done" << std::endl;

  std::cout << param << std::endl;

  bool needZoom = (param.zoomFactor != 1.f);

  std::cout << "Output file: " << szOutputFilename << std::endl;

  cl_mem bufSeeds = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(cl_uint)*param.workers, nullptr, nullptr);
  cl_mem bufPos = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(cl_float2)*param.Nparticles, nullptr, nullptr);
  cl_mem bufState = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(cl_uchar)*param.Nparticles, nullptr, nullptr);

  std::cout << "Seeding... " << std::flush;

  cl_uint* pSeeds = new cl_uint[param.workers];

  srand(param.seed);

  // We initialise the seed buffer
  for(unsigned int i = 0; i < param.workers; i++)
    pSeeds[i] = rand();

  OCLCheck(clEnqueueWriteBuffer(commandQueue, bufSeeds, CL_TRUE, 0, sizeof(cl_uint)*param.workers, static_cast<const void*>(pSeeds), 0, nullptr, nullptr));

  delete[] pSeeds;

  std::cout << "Done" << std::endl;

  std::cout << "Initial conditions..." << std::flush;
  myPerfTimer = Timer();

  clerror = init_homogeneous(context, commandQueue, deviceId, bufSeeds, bufPos, bufState, param.workers, param.particlesPerWorker, param.pi, param.dimxy);

  if(clerror != CL_SUCCESS)
    return -1;

  std::cout << "Done. Elapsed time: " << myPerfTimer.Elapsed() << "s" << std::endl;

  if(needZoom)
  {
    std::cout << "Transforming particles into zoomed space..." << std::flush;

    if(zoom(context, commandQueue, deviceId, bufPos, param.workers, param.particlesPerWorker, param.dimxy, param.zoomFactor) != CL_SUCCESS)
      return -1;

    std::cout << "Done" << std::endl;
  }

  std::cout << "Bleaching..." << std::endl;
  Timer timerFullSimulation;
  Timer timerBleaching;

  #ifdef DEBUG_BLEACH_FRAMES
  std::vector<Eigen::ArrayXXi> bleachFrames;
  #endif

  // We now bleach bleachFrame time
  for(unsigned int i = 0; i < param.bleachFrames; i++)
  {
    Timer timerFrame;
    std::cout << "Bleaching frame " << i << "... " << std::flush;

    // Because of a bug with the Nvidia driver, we have to create/release the mask buffer for each frame. Investigation is needed in order to find a better solution.
    cl_mem bufBleachProfile = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(cl_float)*param.dimxy[0]*param.dimxy[1], nullptr, nullptr);

    // We fill the bleach profile buffer
    if(param.bleachType == "disk")
      clerror = create_mask_disk(context, commandQueue, deviceId, bufBleachProfile, param.dimxy, param.bleachRadius, param.bleachCoef, 1.f, 2);
    else if(param.bleachType == "gaussian")
      clerror = create_mask_gaussian(context, commandQueue, deviceId, bufBleachProfile, param.dimxy, param.bleachRadius, param.bleachCoef, 1.f);
    else if(param.bleachType == "rect")
      clerror = create_mask_rect(context, commandQueue, deviceId, bufBleachProfile, param.dimxy, param.bleachRect, param.bleachCoef, 1.f, 2);
    else
      clerror = create_mask_rect(context, commandQueue, deviceId, bufBleachProfile, param.dimxy, param.bleachRect, 1.f, 1.f, 1);
    
    if(clerror != CL_SUCCESS)
      return -1;

    // Bleaching
    if(sssdb_bleach_one_frame(context, commandQueue, deviceId, bufSeeds, bufPos, bufState, bufBleachProfile, param.workers, param.particlesPerWorker, param.D, param.mu, param.PDT, param.BTOT, param.NLT, param.simSteps, param.pixelStride, param.dimxy, param.zoomFactor, param.db) != CL_SUCCESS)
      return -1;

    clReleaseMemObject(bufBleachProfile);

    #ifdef DEBUG_BLEACH_FRAMES
    Eigen::ArrayXXi curBleachFrame;
    get_frame_from_particles(context, commandQueue, deviceId, bufPos, bufState, param.workers, param.particlesPerWorker, param.dimxy, curBleachFrame);
    bleachFrames.push_back(curBleachFrame);
    #endif

    std::cout << "Done. Elapsed time: " << timerFrame.Elapsed() << "s" << std::endl;

    #ifdef WITH_CONSOLIDATION
    std::cout << "Consolidation pass..." << std::flush;
    Timer timerConsolidation;

    unsigned int oldParticlesPerWorker = param.particlesPerWorker;
    param.particlesPerWorker = consolidate_active_particles(commandQueue, bufPos, bufState, param.workers, param.particlesPerWorker);

    std::cout << "Done. Elapsed time: " << timerConsolidation.Elapsed() << "s" << std::endl;
    std::cout << "New particlesPerWorker = " << param.particlesPerWorker << " (lost " << (1.f-(float)param.particlesPerWorker/oldParticlesPerWorker)*100.f << "%)" << std::endl;
    #endif

  }
  
  #ifdef DEBUG_BLEACH_FRAMES
  export_csv<Eigen::ArrayXXi>("bleach_" + szOutputFilename, bleachFrames);
  #endif

  std::cout << "Done bleaching. Elapsed time: " << timerBleaching.Elapsed() << "s" << std::endl;

  if(needZoom)
  {
    std::cout << "Transforming particles back into unzoomed space..." << std::flush;

    if(unzoom(context, commandQueue, deviceId, bufPos, param.workers, param.particlesPerWorker, param.dimxy, param.zoomFactor) != CL_SUCCESS)
      return -1;

    std::cout << "Done" << std::endl;
  }

  std::vector<Eigen::ArrayXXi> Ct;

  cl_int* pCt = new cl_int[param.dimxy[0]*param.dimxy[1]];
  cl_int* pEmptyBuffer = new cl_int[param.dimxy[0]*param.dimxy[1]];

  memset(pEmptyBuffer, 0x00, sizeof(cl_uint)*param.dimxy[0]*param.dimxy[1]);

  cl_mem bufBin = clCreateBuffer(context, CL_MEM_WRITE_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_int)*param.dimxy[0]*param.dimxy[1], pEmptyBuffer, nullptr);

  std::cout << "Simulating..." << std::endl;
  
  Timer timerSimulation;
  
  for(unsigned int i = 0; i < param.frames; i++)
  {
    Timer timerFrame;
    std::cout << "Simulating frame " << i << "..." << std::flush;
    
    if(sssdb_diffuse_one_frame(context, commandQueue, deviceId, bufSeeds, bufPos, bufState, bufBin, param.workers, param.particlesPerWorker, param.D, param.mu, param.bleachCoef, param.PDT, param.BTOT, param.NLT, param.simSteps, param.pixelStride, param.dimxy, param.db) != CL_SUCCESS)
      return -1;

    OCLCheck(clEnqueueReadBuffer(commandQueue, bufBin, CL_TRUE, 0, sizeof(cl_uint)*param.dimxy[0]*param.dimxy[1], pCt, 0, nullptr, nullptr));
    Ct.push_back(Eigen::Map<Eigen::ArrayXXi>(pCt, param.dimxy[0], param.dimxy[1]));

    OCLCheck(clEnqueueWriteBuffer(commandQueue, bufBin, CL_TRUE, 0, sizeof(cl_uint)*param.dimxy[0]*param.dimxy[1], pEmptyBuffer, 0, nullptr, nullptr));

    std::cout << "Done. Elapsed time: " << timerFrame.Elapsed() << std::endl;
  }

  std::cout << "Done simulating. Elapsed time: " << timerSimulation.Elapsed() << std::endl;
  std::cout << "Total elapsed time: " << timerFullSimulation.Elapsed() << std::endl;

  delete[] pCt;
  delete[] pEmptyBuffer;
		 
  std::cout << "Saving result to " << szOutputFilename << ". " << std::flush;
  export_csv<Eigen::ArrayXXi>(szOutputFilename, Ct);
  std::cout << "Done" << std::endl;

  std::cout << "Cleaning..." << std::flush;
  OCLCheck(clReleaseMemObject(bufSeeds));
  OCLCheck(clReleaseMemObject(bufPos));
  OCLCheck(clReleaseMemObject(bufState));
  OCLCheck(clReleaseMemObject(bufBin));
  OCLCheck(clReleaseCommandQueue(commandQueue));
  OCLCheck(clReleaseContext(context));
  std::cout << "Done" << std::endl;

  return 0;
}
