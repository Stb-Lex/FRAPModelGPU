import numpy as np
import matplotlib.pyplot as plt
import sys
import skimage.measure
import common

filename_s1 = sys.argv[1]
filename_s2 = sys.argv[2]

print("Loading data...")
data_s1, dim_s1, n_s1 = common.load_frames_from_file(filename_s1)
data_s2, dim_s2, n_s2 = common.load_frames_from_file(filename_s2)

if(n_s1 != n_s2):
    print("Error! Frame count not equal!")
    exit()

dim = dim_s1
n = n_s1
    
for i in range(0, n):
    plt.subplot(1, 2, 1)
    plt.title(i)
    plt.plot(range(0, dim[0]), data_s1[:,int(dim[1]/2),i])
    plt.plot(range(0, dim[0]), data_s2[:,int(dim[1]/2),i])
    plt.subplot(1, 2, 2)
    plt.plot(range(0, dim[0]), data_s1[:,int(dim[1]/2),i] - data_s2[:,int(dim[1]/2),i])
    plt.show()
