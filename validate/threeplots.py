import numpy as np
import matplotlib.pyplot as plt
import sys
import common

data, dim, n = common.load_frames_from_file(sys.argv[1])

s = np.zeros(n)

plt.subplot(1, 3, 1)
plt.title("Frame 1")
plt.imshow(data[:,:,0], cmap='gray')

plt.subplot(1, 3, 2)
plt.title("Frame 5")
plt.imshow(data[:,:,4], cmap='gray')

plt.subplot(1, 3, 3)
plt.title("Frame 15")
plt.imshow(data[:,:,14], cmap='gray')

plt.show()
