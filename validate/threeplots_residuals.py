import numpy as np
import matplotlib.pyplot as plt
import sys
import common

data1, dim, n = common.load_frames_from_file(sys.argv[1])
data2, dim, n = common.load_frames_from_file(sys.argv[2])

normalize = False
if(len(sys.argv) == 4):
    normalize = bool(sys.argv[3])

if(normalize):
    print("Normalizing...")
    for i in range(0, n):
        data1[:,:,i] = data1[:,:,i]/np.sum(data1[:,:,i])
        data2[:,:,i] = data2[:,:,i]/np.sum(data2[:,:,i])


res = (data1 - data2)/data2

plt.subplot(2, 2, 1)
plt.title("Frame 1")
plt.imshow(res[:,:,0], vmin = np.min(res), vmax = np.max(res), cmap="jet")

plt.subplot(2, 2, 2)
plt.title("Frame 5")
plt.imshow(res[:,:,4], vmin = np.min(res), vmax = np.max(res), cmap="jet")

plt.subplot(2, 2, 3)
plt.title("Frame 15")
plt.imshow(res[:,:,14], vmin = np.min(res), vmax = np.max(res), cmap="jet")

plt.subplot(2, 2, 4)
plt.title("Frame 30")
plt.imshow(res[:,:,29], vmin = np.min(res), vmax = np.max(res), cmap="jet")

plt.subplots_adjust(bottom=0.1, right=0.8, top=0.9)
cax = plt.axes([0.85, 0.3, 0.025, 0.4])

plt.colorbar(cax=cax)

plt.show()

print("Mean value of frame 30:", np.mean(res[:,:,29]))
