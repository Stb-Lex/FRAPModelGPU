import numpy as np
import matplotlib.pyplot as plt
import sys
import common

data, dim, n = common.load_frames_from_file(sys.argv[1])

print(n)

logscale = False
if(len(sys.argv) == 3):
    logscale = bool(sys.argv[2])

s = np.zeros(n)
    
for i in range(0, n):
    print('Frame', i)
    print('Max:', np.max(data[:,:,i]))
    print('Avg:', np.mean(data[:,:,i]))
    print('Sum:', np.sum(data[:,:,i]))

    s[i] = np.sum(data[:,:,i])

    plt.title(i)
    #plt.subplot(1, 2, 1)
    if(logscale == True):
        #plt.imshow(np.log(data[:,:,i]), cmap='gray', vmin=np.min(data), vmax=np.max(data))
        plt.imshow(np.log(data[:,:,i]), cmap='gray')
    else:
        #plt.imshow(data[:,:,i], cmap='gray', vmin=np.min(data), vmax=np.max(data))
        plt.imshow(data[:,:,i], cmap='gray')
    #plt.subplot(1, 2, 2)
    #plt.plot(range(0, dim[1]), np.sum(data[:,:,i], axis=0))
    #plt.plot(range(0, dim[1]), data[int(dim[0]/2), :, i])
    plt.show()

    #plt.hist(data[:,:,i].flatten(), 250)
    #plt.show()


plt.plot(range(0, n), s)
plt.show()
