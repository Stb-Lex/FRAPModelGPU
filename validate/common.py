import pickle
import os
import numpy as np

class Frames:
    def __init__(self, data, dimxy, frames):
        self.data = data
        self.dimxy = dimxy
        self.frames = frames

def export_pickle(filename, data, dimxy, frames):
    frames = Frames(data, dimxy, frames)
    with open(filename, 'wb') as f:
        pickle.dump(frames, f)

def load_frames_from_file(_filename):
    filename, file_extension = os.path.splitext(_filename)

    if(file_extension == ".csv"):
        strSettings = ''
        with open(_filename, 'r') as f:
            strSettings = f.readline()

        dataSettings = np.fromstring(strSettings, dtype=int, sep=',')
        dim = dataSettings[0:2]
        n = dataSettings[2]
        rawdata = np.loadtxt(_filename, delimiter=",", skiprows=1)
        data = np.zeros([dim[0], dim[1], n])
        for i in range(0, n):
            data[:,:,i] = rawdata[(i*dim[0]):((i+1)*dim[0]), :]

        return data, dim, n
        
    elif(file_extension == ".pickle"):
        with open(_filename, 'rb') as f:
            frames = pickle.load(f, encoding='latin1')

        return frames.data, frames.dimxy, frames.frames

    else:
        print("Error! Could not recognise file extension!")
        return None
    
