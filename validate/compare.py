import numpy as np
import matplotlib.pyplot as plt
import common
import sys
import skimage.measure
import scipy.stats

def symmetry_metric(A, norm=None):
    Asym = 0.5*(A+A.T)
    Aanti = 0.5*(A-A.T)

    return(np.linalg.norm(Asym, ord=norm)-np.linalg.norm(Aanti, ord=norm))/(np.linalg.norm(Asym, ord=norm)+np.linalg.norm(Aanti, ord=norm))

filename_s1 = sys.argv[1]
filename_s2 = sys.argv[2]

print("Loading data...")
data_s1, dim_s1, n_s1 = common.load_frames_from_file(filename_s1)
data_s2, dim_s2, n_s2 = common.load_frames_from_file(filename_s2)

if(n_s1 != n_s2):
    print("Error! Frame count not equal!")
    exit()

dim = dim_s1
n = n_s1

normalize = False
if(len(sys.argv) == 4):
    normalize = bool(sys.argv[3])

if(normalize):
    print("Normalizing...")
    for i in range(0, n):
        data_s1[:,:,i] = data_s1[:,:,i]/np.sum(data_s1[:,:,i])
        data_s2[:,:,i] = data_s2[:,:,i]/np.sum(data_s2[:,:,i])

print("Computing residuals...")
res = (data_s1 - data_s2)/data_s2

print('Mu:', np.mean(res), 'Sigma:', np.sqrt(np.var(res)))
print('Max |res|', np.max(np.abs(res)))

sk = np.zeros(n)

for i in range(0, n):
    print("Frame #", i)
    moments = skimage.measure.moments(res[:,:,i], order=3)
    print("Central moments:")
    print(moments)
    print("Symmetry:", symmetry_metric(moments, 2))
    plt.subplot(2, 2, 1)
    plt.imshow(data_s1[:,:,i])
    plt.subplot(2, 2, 2)
    plt.imshow(data_s2[:,:,i])
    plt.subplot(2, 2, 3)
    plt.imshow(res[:,:,i])
    plt.subplot(2, 2, 4)
    plt.hist(res[:,:,i].flatten(), 100)
    plt.show()

    sk[i] = scipy.stats.skew(res[:,:,i].flatten())

plt.plot(range(0, n), sk)
plt.show()
