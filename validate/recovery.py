import numpy as np
import matplotlib.pyplot as plt
import sys
import common
import scipy.optimize

def f(x, A, B, tau):
    return A*(1-B*np.exp(-tau*x))

filename = sys.argv[1]
radius = float(sys.argv[2])

data, dim, n = common.load_frames_from_file(filename)
    
print('Computing recovery curve...')
X, Y = np.ogrid[-(dim[0]/2):(dim[0]/2), -(dim[1]/2):(dim[1]/2)]
mask = X**2 + Y**2 <= radius**2

x = range(0, n)
y = np.zeros(n)
for i in x:
    y[i] = np.sum(data[:,:,i]*mask)/(np.pi*radius*radius)

print('Fitting...')
popt, pcov = scipy.optimize.curve_fit(f, x, y)
tau = popt[2]
print('Tau:', tau)
print('Computed D:', radius*radius/4/tau)

plt.subplot(1, 2, 1)
plt.xlabel('Frame #')
plt.ylabel('I')
plt.plot(x, y)
plt.plot(x, f(x, popt[0], popt[1], popt[2]))

plt.subplot(1, 2, 2)
plt.xlabel('Frame #')
plt.ylabel('residuals')
plt.plot(y - f(x, popt[0], popt[1], popt[2]))

plt.show()


