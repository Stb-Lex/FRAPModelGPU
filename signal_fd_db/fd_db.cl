__kernel void fd_db(__global float* C)
{
	uint2 posij = (uint2)(get_global_id(0), get_global_id(1));
	uint posId = posij.y*get_global_size(0)+posij.x;

	uint2 posim1j = (uint2)((posij.x-1)%get_global_size(0), posij.y);
	uint2 posip1j = (uint2)((posij.x+1)%get_global_size(0), posij.y);
	uint2 posijm1 = (uint2)(posij.x, (posij.y-1)%get_global_size(1));
	uint2 posijp1 = (uint2)(posij.x, (posij.y+1)%get_global_size(1));

	float U = PION*C[posId];
	float B = PIOFF*C[posId];

	float Uim1 = PION*C[posim1j.y*get_global_size(0)+posim1j.x];
	float Uip1 = PION*C[posip1j.y*get_global_size(0)+posip1j.x];
	float Ujm1 = PION*C[posijm1.y*get_global_size(1)+posijm1.x];
	float Ujp1 = PION*C[posijp1.y*get_global_size(1)+posijp1.x];

	float Unext = U + DT*(D*(Uim1+Uip1+Ujm1+Ujp1-4.f*U)-KON*U+KOFF*B);
	float Bnext = B + DT*(KON*U-KOFF*B);

	C[posId] = Unext+Bnext;
}
