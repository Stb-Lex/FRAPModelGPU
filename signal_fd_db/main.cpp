#include<CL/opencl.h>
#include<iostream>
#include<Eigen/Dense>

#include"signal_fd_db.h"
#include"initial_db.h"
#include"export_solution.h"
#include"timer.h"
#include"opencl_common.h"


int main(int argc, char* argv[])
{
  if(argc == 1)
  {
    std::cout << "Usage (disk): signal_db outFilename steps disk radius" << std::endl;
    std::cout << "Usage (rectangle): signal_db outFilename steps rect dimx dimy" << std::endl;
    return 0;
  }

  std::string szOutFilename = argv[1];

  char bleachType;

  if(strcmp(argv[3], "disk") == 0)
    bleachType = 'd';
  else if(strcmp(argv[3], "rect") == 0)
    bleachType = 'r';
  else if(strcmp(argv[3], "gaussian") == 0)
    bleachType = 'g';
  else
  {
    std::cerr << "Bleaching type unrecognised... Bye bye" << std::endl;
    return -1;
  }

  // Problem related constants
  const float Iu = 1.0f;
  const float Ib = 0.6;
  const cl_float2 k = {1.f, 1.f}; // kon, koff
  const float D = 500.f;

  const unsigned int N = 256;
  const unsigned int Pad = 128;
  const unsigned int n = atoi(argv[2]); // Number of slices/time steps
  const float dT = 0.05f; // s
  const unsigned int simSteps = 4096;
  const float simdT = dT/(float)simSteps;

  const size_t dimxy[2] = {2*Pad+N, 2*Pad+N};

  // OpenCL related vars  
  cl_platform_id platformId;
  cl_device_id deviceId;
  cl_context context;
  cl_command_queue commandQueue;
  cl_int clerror;
  unsigned int deviceCount;
  unsigned int platformCount;

  // We initialise OpenCL
  Timer myPerfTimer;
  std::cout << "Initialising OpenCL... " << std::flush;

  clerror = initOpenCL(platformId, context, deviceId, commandQueue, CL_DEVICE_TYPE_GPU);

  if(clerror != CL_SUCCESS)
    return -1;

  std::cout << "Done. Elapsed time: " << myPerfTimer.Elapsed() << "s" << std::endl;

  std::cout << "Generating initial conditions..." << std::flush;
  myPerfTimer = Timer();

  cl_mem bufC0 = nullptr;
  
  std::vector<Eigen::ArrayXXf> Ct;
  
  cl_uint2 diskPos = {0, 0};
  if(bleachType == 'd')
  {
    float bleachRadius = (float)atoi(argv[4]);
    clerror = init_disk_gpu(context, commandQueue, deviceId, bufC0, dimxy, bleachRadius, Ib, Iu, 2);
  }
  else if(bleachType == 'r')
  {
    cl_float2 bleachRect;
    bleachRect.s[0] = (float)atoi(argv[4]);
    bleachRect.s[1] = (float)atoi(argv[5]);
    clerror = init_rect_gpu(context, commandQueue, deviceId, bufC0, dimxy, bleachRect, Ib, Iu, 2);
  }
  else if(bleachType == 'g')
  {
    float bleachRadius = (float)atoi(argv[4]);
    clerror = init_gaussian_gpu(context, commandQueue, deviceId, bufC0, dimxy, bleachRadius, Ib, Iu);
  }

  if(clerror != CL_SUCCESS)
    return -1;

  std::cout << "Done. Elapsed time: " << myPerfTimer.Elapsed() << "s" << std::endl;

  std::cout << "Simulating..." << std::flush;
  myPerfTimer = Timer();
  clerror = signal_fd_db_euler(context, commandQueue, deviceId, bufC0, k, D, simdT, dimxy, n, simSteps, Ct);
  std::cout << "Done. Elapsed time: " << myPerfTimer.Elapsed() << "s" << std::endl;

  std::cout << "Saving on disk under " << szOutFilename << "..." << std::flush;
  export_csv<Eigen::ArrayXXf>(szOutFilename, Ct);
  std::cout << "Done" << std::endl;

  OCLCheck(clReleaseMemObject(bufC0));
  OCLCheck(clReleaseCommandQueue(commandQueue));
  OCLCheck(clReleaseContext(context));
  
  return 0;
}
