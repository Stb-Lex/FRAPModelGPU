#ifndef SIGNAL_FD_DB
#define SIGNAL_FD_DB

#include<CL/opencl.h>
#include<vector>
#include<Eigen/Dense>

cl_int signal_fd_db_euler(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufC0, const cl_float2& k, const float D, const float dT, const size_t dimxy[2], const unsigned int n, unsigned int simSteps, std::vector<Eigen::ArrayXXf>& Ct);

#endif // SIGNAL_FD_DB
