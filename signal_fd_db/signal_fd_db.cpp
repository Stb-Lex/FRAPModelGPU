#include"signal_fd_db.h"
#include"opencl_common.h"
#include<iostream>
#include<cmath>

cl_int signal_fd_db_euler(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufC0, const cl_float2& k, const float D, const float dT, const size_t dimxy[2], const unsigned int n, const unsigned int simSteps, std::vector<Eigen::ArrayXXf>& Ct)
{
  cl_int clerror;

  cl_program fd_db_program;
  cl_kernel fd_db_kernel;

  ParameterList dbParameters;
  dbParameters.push_back(Parameter("PION", k.s[1]/(k.s[0]+k.s[1])));
  dbParameters.push_back(Parameter("PIOFF", k.s[0]/(k.s[0]+k.s[1])));
  dbParameters.push_back(Parameter("KON", k.s[0]));
  dbParameters.push_back(Parameter("KOFF", k.s[1]));
  dbParameters.push_back(Parameter("D", D));
  dbParameters.push_back(Parameter("DT", dT));

  clerror = loadKernel("../signal_fd_db/fd_db.cl", "fd_db", "", context, deviceId, &fd_db_program, &fd_db_kernel, &dbParameters);

  if(clerror != CL_SUCCESS)
    return clerror;

  OCLCheck(clSetKernelArg(fd_db_kernel, 0, sizeof(cl_mem), &bufC0));
  
  float* pCt = new float[dimxy[0]*dimxy[1]];

  // We simulate n frames
  for(unsigned int i = 0; i < n; i++)
  {
    for(unsigned int j = 0; j < simSteps; j++)
    {
      OCLCheck(clEnqueueNDRangeKernel(commandQueue, fd_db_kernel, 2, nullptr, dimxy, nullptr, 0, nullptr, nullptr));
      OCLCheck(clFinish(commandQueue));
    }

    OCLCheck(clEnqueueReadBuffer(commandQueue, bufC0, CL_TRUE, 0, sizeof(cl_float)*dimxy[0]*dimxy[1], pCt, 0, nullptr, nullptr));
    Ct.push_back(Eigen::Map<Eigen::ArrayXXf>(pCt, dimxy[0], dimxy[1]));
  }

  delete[] pCt;

  OCLCheck(clReleaseKernel(fd_db_kernel));
  OCLCheck(clReleaseProgram(fd_db_program));

  return CL_SUCCESS;
}


