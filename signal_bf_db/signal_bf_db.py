import numpy as np
import matplotlib.pyplot as plt
import sys
import scipy.misc
import scipy.ndimage

sys.path.append("../validate/")
import common

N = 256
M = 128
dimxy = [N+2*M, N+2*M]
frames = 500
bleachFrames = 4
upscale = 1

k = [1., 1.]
pi = [k[1]/(k[0]+k[1]), k[0]/(k[0]+k[1])]
D = 1000.
dT = 0.05
bfdT = 0.1

R = 64
alpha = 0.9

Y, X = np.ogrid[-upscale*(dimxy[0]/2):upscale*(dimxy[0]/2), -upscale*(dimxy[1]/2):upscale*(dimxy[1]/2)]
X = X + 0.5
Y = Y + 0.5
diskMask = X*X+Y*Y <= upscale*upscale*R*R
bleachMask = np.ones([upscale*dimxy[0], upscale*dimxy[1]])
bleachMask[diskMask] = alpha

# Then we downscale
#bleachMask = scipy.ndimage.zoom(bleachMask, 1/upscale, order=2, prefilter=True)
#bleachMask = scipy.misc.imresize(bleachMask, dimxy, mode='F')

Y, X = np.ogrid[-(dimxy[0]/2):(dimxy[0]/2), -(dimxy[1]/2):(dimxy[1]/2)]
X = X*2*np.pi/dimxy[0]
Y = Y*2*np.pi/dimxy[1]

XSISQ = np.fft.ifftshift(X*X+Y*Y)

QQ11 = -(k[0] - k[1] + (D**2*XSISQ**2 + 2*D*k[0]*XSISQ - 2*D*k[1]*XSISQ + k[0]**2 + 2*k[0]*k[1] + k[1]**2)**(1/2) + D*XSISQ)/(2*k[0])
QQ12 = -(k[0] - k[1] - (D**2*XSISQ**2 + 2*D*k[0]*XSISQ - 2*D*k[1]*XSISQ + k[0]**2 + 2*k[0]*k[1] + k[1]**2)**(1/2) + D*XSISQ)/(2*k[0])

LL11 = -((k[0] + k[1] + (D**2*XSISQ**2 + 2*D*k[0]*XSISQ - 2*D*k[1]*XSISQ + k[0]**2 + 2*k[0]*k[1] + k[1]**2)**(1/2) + D*XSISQ))/2
LL22 = -((k[0] + k[1] - (D**2*XSISQ**2 + 2*D*k[0]*XSISQ - 2*D*k[1]*XSISQ + k[0]**2 + 2*k[0]*k[1] + k[1]**2)**(1/2) + D*XSISQ))/2

QQinv11 = -k[0]/(2*k[0]*k[1] + k[0]**2 + k[1]**2 + D**2*XSISQ**2 + 2*D*k[0]*XSISQ - 2*D*k[1]*XSISQ)**(1/2)
QQinv12 = -(k[0] - k[1] - (D**2*XSISQ**2 + 2*D*k[0]*XSISQ - 2*D*k[1]*XSISQ + k[0]**2 + 2*k[0]*k[1] + k[1]**2)**(1/2) + D*XSISQ)/(2*(2*k[0]*k[1] + k[0]**2 + k[1]**2 + D**2*XSISQ**2 + 2*D*k[0]*XSISQ - 2*D*k[1]*XSISQ)**(1/2))
QQinv21 = -QQinv11
QQinv22 = (k[0] - k[1] + (D**2*XSISQ**2 + 2*D*k[0]*XSISQ - 2*D*k[1]*XSISQ + k[0]**2 + 2*k[0]*k[1] + k[1]**2)**(1/2) + D*XSISQ)/(2*(2*k[0]*k[1] + k[0]**2 + k[1]**2 + D**2*XSISQ**2 + 2*D*k[0]*XSISQ - 2*D*k[1]*XSISQ)**(1/2))

C0 = np.ones(dimxy)
U0 = pi[0]*C0
B0 = pi[1]*C0

# Bleaching
data_bleaching = np.zeros([dimxy[0], dimxy[1], bleachFrames])
for t in range(0, bleachFrames):

    U0 = bleachMask*U0
    B0 = bleachMask*B0

    F_U0 = np.fft.fft2(U0)
    F_B0 = np.fft.fft2(B0)

    CONST11 = QQ11 * (QQinv11 * F_U0 + QQinv12 * F_B0)
    CONST12 = QQ12 * (QQinv21 * F_U0 + QQinv22 * F_B0)
    CONST21 = QQinv11 * F_U0 + QQinv12 * F_B0
    CONST22 = QQinv21 * F_U0 + QQinv22 * F_B0

    CONST1 = np.exp(LL11 * bfdT)
    CONST2 = np.exp(LL22 * bfdT)
    F_U0 = CONST11 * CONST1 + CONST12 * CONST2
    F_B0 = CONST21 * CONST1 + CONST22 * CONST2

    U0 = np.abs(np.fft.ifft2(F_U0))
    B0 = np.abs(np.fft.ifft2(F_B0))

    data_bleaching[:,:,t] = U0 + B0

common.export_pickle("bleach_"+sys.argv[1], data_bleaching, dimxy, bleachFrames)
    
F_U0 = np.fft.fft2(U0)
F_B0 = np.fft.fft2(B0)
    
CONST11 = QQ11 * (QQinv11 * F_U0 + QQinv12 * F_B0)
CONST12 = QQ12 * (QQinv21 * F_U0 + QQinv22 * F_B0)
CONST21 = QQinv11 * F_U0 + QQinv12 * F_B0
CONST22 = QQinv21 * F_U0 + QQinv22 * F_B0

F_Ut = np.zeros([dimxy[0], dimxy[1], frames], dtype=np.complex64)
F_Bt = np.zeros([dimxy[0], dimxy[1], frames], dtype=np.complex64)

data = np.zeros([dimxy[0], dimxy[1], frames])

# Simulating
for t in range(0, frames):
    T = (t+1) * dT
    CONST1 = np.exp(LL11 * T)
    CONST2 = np.exp(LL22 * T)
    F_Ut = CONST11 * CONST1 + CONST12 * CONST2
    F_Bt = CONST21 * CONST1 + CONST22 * CONST2

    data[:, :, t] = np.abs(np.fft.ifft2(F_Ut + F_Bt))

common.export_pickle(sys.argv[1], data, dimxy, frames)
