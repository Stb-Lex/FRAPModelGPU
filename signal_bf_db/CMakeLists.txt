add_executable(signal_bf_db main.cpp ../signal_db/signal_db.cpp ../common/opencl_common.cpp ../common/mask.cpp)

target_include_directories(signal_bf_db PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_include_directories(signal_bf_db PUBLIC "../common/")
target_include_directories(signal_bf_db PUBLIC "../signal_db/")

target_link_libraries(signal_bf_db ${OpenCL_LIBRARY})
target_link_libraries(signal_bf_db Eigen3::Eigen)
target_link_libraries(signal_bf_db ${CLFFT_LIBRARIES})
target_link_libraries(signal_bf_db config++)
