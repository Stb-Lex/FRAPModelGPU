#include"opencl_common.h"
#include"timer.h"
#include"signal_db.h"
#include"export_solution.h"
#include"mask.h"

#include<iostream>
#include<clFFT.h>
#include<string>
#include<cstring>

#define DEBUG_BLEACH_FRAMES

int main(int argc, char* argv[])
{
  if(argc == 1)
  {
    std::cout << "Usage (disk): signal_bf_db outFilename steps disk radius" << std::endl;
    std::cout << "Usage (rectangle): signal_bf_db outFilename steps rect dimx dimy" << std::endl;
    return 0;
  }

  std::string szOutFilename = argv[1];

  char bleachType;

  if(strcmp(argv[3], "disk") == 0)
    bleachType = 'd';
  else if(strcmp(argv[3], "rect") == 0)
    bleachType = 'r';
  else if(strcmp(argv[3], "gaussian") == 0)
    bleachType = 'g';
  else
  {
    std::cerr << "Bleaching type unrecognised... Bye bye" << std::endl;
    return -1;
  }
  
  // Problem related constants
  const float alpha = 0.7f;
  const float k[2] = {1.f, 1.f}; // kon, koff
  const float pi[2] = {k[1]/(k[0]+k[1]), k[0]/(k[0]+k[1])};
  const float D = 800.f;
  const unsigned int bleachFrame = 4;
  const float dT = 0.265f; // s
  const float bleachFramedT = 0.265f;

  const bool db = true;

  const unsigned int N = 256;
  const unsigned int Pad = 128;
  const unsigned int n = atoi(argv[2]); // Number of slices/time steps

  const size_t dimxy[2] = {N+2*Pad, N+2*Pad};

  // OpenCL related vars  
  cl_platform_id platformId;
  cl_device_id deviceId;
  cl_context context;
  cl_command_queue commandQueue;
  unsigned int deviceCount;
  unsigned int platformCount;
  cl_int clerror;

  // We initialise OpenCL and clFFT
  Timer myPerfTimer;
  std::cout << "Initialising OpenCL and clFFT... " << std::flush;

  clerror = initOpenCL(platformId, context, deviceId, commandQueue, CL_DEVICE_TYPE_GPU);
  //clerror = initOpenCLByPlatformName(platformId, context, deviceId, commandQueue, "Portable Computing Language", CL_DEVICE_TYPE_CPU);

  if(clerror != CL_SUCCESS)
    return -1;

  clfftSetupData fftSetup;
  clerror = clfftInitSetupData(&fftSetup);
  clerror = clfftSetup(&fftSetup);

  if(clerror != CL_SUCCESS)
  {
    std::cerr << "clFFT: Could not setup! Error code: " << clerror << std::endl;
    return -1;
  }

  std::cout << "Done. Elapsed time: " << myPerfTimer.Elapsed() << "s" << std::endl;

  // All the buffers used by the simulation
  
  cl_mem bufC = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(cl_float)*dimxy[0]*dimxy[1], nullptr, nullptr);

  cl_mem bufU[2];
  cl_mem bufB[2];

  cl_float* pU0 = new cl_float[dimxy[0]*dimxy[1]];
  cl_float* pB0 = new cl_float[dimxy[0]*dimxy[1]];

  for(unsigned int i = 0; i < dimxy[0]*dimxy[1]; i++)
  {
    pU0[i] = pi[0];
    pB0[i] = pi[1];
  }

  bufU[0] = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof(cl_float)*dimxy[0]*dimxy[1], pU0, nullptr);
  bufU[1] = createEmptyBuffer(context, CL_MEM_READ_WRITE, sizeof(cl_float)*dimxy[0]*dimxy[1]);
  bufB[0] = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof(cl_float)*dimxy[0]*dimxy[1], pB0, nullptr);
  bufB[1] = createEmptyBuffer(context, CL_MEM_READ_WRITE, sizeof(cl_float)*dimxy[0]*dimxy[1]);

  delete[] pU0;
  delete[] pB0;
  
  cl_mem bufBleachProfile = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(cl_float)*dimxy[0]*dimxy[1], nullptr, nullptr);
  
  std::cout << "Bleaching..." << std::endl;
  myPerfTimer = Timer();

  // We fill the bleach profile buffer
  if(bleachType == 'd')
  {
    float bleachRadius = (float)atoi(argv[4]);
    clerror = create_mask_disk(context, commandQueue, deviceId, bufBleachProfile, dimxy, bleachRadius, alpha, 1.0f, 2);
  }
  else if(bleachType == 'r')
  {
    float bleachRect[2];
    bleachRect[0] = (float)atoi(argv[4]);
    bleachRect[1] = (float)atoi(argv[5]);
    clerror = create_mask_rect(context, commandQueue, deviceId, bufBleachProfile, dimxy, bleachRect, alpha, 1.0f, 2);
  }
  else
  {
    float bleachRadius = (float)atoi(argv[4]);
    clerror = create_mask_gaussian(context, commandQueue, deviceId, bufBleachProfile, dimxy, bleachRadius, alpha, 1.0f);
  }

  if(clerror != CL_SUCCESS)
    return -1;

  // We initialise the bleaching kernel
  cl_kernel bleach_kernel;
  cl_program bleach_program;

  ParameterList bleachParameters;
  
  clerror = loadKernel("../common_cl/spectral_bleach.cl", "bleach", "", context, deviceId, &bleach_program, &bleach_kernel, &bleachParameters);

  OCLCheck(clSetKernelArg(bleach_kernel, 0, sizeof(cl_mem), &bufU[0]));
  OCLCheck(clSetKernelArg(bleach_kernel, 1, sizeof(cl_mem), &bufB[0]));
  OCLCheck(clSetKernelArg(bleach_kernel, 2, sizeof(cl_mem), &bufBleachProfile));

  #ifdef DEBUG_BLEACH_FRAMES
  std::vector<Eigen::ArrayXXf> bleachFrames;
  #endif

  for(unsigned int i = 0; i < bleachFrame; i++)
  {
    std::cout << "Frame " << i << "..." << std::flush;
    
    // We first bleach in time domain
    clerror = clEnqueueNDRangeKernel(commandQueue, bleach_kernel, 2, nullptr, dimxy,  nullptr, 0, nullptr, nullptr);

    if(clerror != CL_SUCCESS)
    {
      std::cout << "Could not bleach! OpenCL error code: " << clerror << std::endl;
      return -1;
    }

    OCLCheck(clFinish(commandQueue));

    // We then advance in time in fft domain
    clerror = signal_db_one_frame(context, commandQueue, deviceId, bufU, bufB, bufC, db, k, D, bleachFramedT, dimxy);

    if(clerror != CL_SUCCESS)
    {
      std::cout << "Could not simulate! OpenCL error code: " << clerror << std::endl;
      return -1;
    }

    #ifdef DEBUG_BLEACH_FRAMES
    bleachFrames.push_back(get_frame(commandQueue, bufC, dimxy));
    #endif
    
    std::cout << "Done" << std::endl;
  }

  #ifdef DEBUG_BLEACH_FRAMES
  export_csv<Eigen::ArrayXXf>("bleach_" + szOutFilename, bleachFrames);
  #endif

  std::cout << "Done bleaching. Time elapsed: " << myPerfTimer.Elapsed() << "s" << std::endl;

  // We now do the simulation the same way as for signal_db
  std::vector<Eigen::ArrayXXf> Ct;
  
  std::cout << "Simulating..." << std::flush;
  myPerfTimer = Timer();
  clerror = signal_db_fft(context, commandQueue, deviceId, bufU, bufB, db, k, D, dT, dimxy, n, Ct);
  std::cout << "Done. Elapsed time: " << myPerfTimer.Elapsed() << "s" << std::endl;

  std::cout << "Saving on disk under " << szOutFilename << "..." << std::flush;
  export_csv<Eigen::ArrayXXf>(szOutFilename, Ct);
  std::cout << "Done" << std::endl;

  clfftTeardown();

  OCLCheck(clReleaseMemObject(bufU[0]));
  OCLCheck(clReleaseMemObject(bufU[1]));
  OCLCheck(clReleaseMemObject(bufB[0]));
  OCLCheck(clReleaseMemObject(bufB[1]));
  OCLCheck(clReleaseMemObject(bufC));
  
  OCLCheck(clReleaseKernel(bleach_kernel));
  OCLCheck(clReleaseProgram(bleach_program));
  OCLCheck(clReleaseCommandQueue(commandQueue));
  OCLCheck(clReleaseContext(context));
  
  return 0;
}
