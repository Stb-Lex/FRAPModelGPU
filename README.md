# How to build
```
mkdir build & cd build
cmake ../.
make
```

# Description
FRAP experiment models implemented on the GPU using OpenCL and C++. For all simulations, usage is given by calling the executable without parameters.

## Deterministic schemes
Solves the reaction-diffusion PDEs using the spectral method, implemented in OpenCL. Most simulations parameters and device selection are hardcoded into the main.cpp.
### signal_db
Single instantaneous bleach frame.

### signal_bf_db
Multiple instantaneous bleach frame.

## Stochastic schemes
Stochastic formulation of the problems. All simulations parameters are set using a .cfg with an exemple given in the simulation.cfg file of the simulation in question. OpenCL device is selected using an other .cfg. Device usable by the computer I used are in cfg/.

### signal_stochastic_db
Single instantaneous bleach frame.

### signal_stochastic_bf_db
Multiple instantaneous bleach frame.

### signal_stochastic_scan_db
Scanning motion simulation.

# Dependency
libconfig++

Eigen3

clFFT

