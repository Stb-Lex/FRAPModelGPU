void __kernel init_rect(__global float* c0)
{
	uint id = get_global_id(1)*get_global_size(0) + get_global_id(0);

	float2 pos = (float2)((float)get_global_id(0) - 0.5f*get_global_size(0) + 0.5f, (float)get_global_id(1) - 0.5f*get_global_size(1) + 0.5f);

	if(pos.x <= 0.5f*RECTDIMX && pos.x >= -0.5f*RECTDIMX && pos.y <= 0.5f*RECTDIMY && pos.y >= -0.5f*RECTDIMY)
		c0[id] = IB;
	else
		c0[id] = IU;


}
