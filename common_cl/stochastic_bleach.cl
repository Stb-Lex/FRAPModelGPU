#include"state.cl"
#include"sample.cl"

__kernel void bleach_particles(__global uint* g_seeds, __global float2* g_pos, __global uchar* g_state, __global const float* g_mask)
{
	uint g_id = get_global_id(0);

	mwc64x_state_t s;
	MWC64X_SeedStreams(&s, g_seeds[g_id], 0);

	for(uint i = 0; i < PARTICLESPERWORKER; i++)
	{
		uint partId = PARTICLESPERWORKER*g_id+i;

		int2 pixelPos = convert_int2_rtz(g_pos[partId]);
		float maskValue = g_mask[pixelPos.y*DIMX+pixelPos.x];

		g_state[partId] = sampleUniform(&s) > maskValue ? g_state[partId] | STATE_INACTIVE : g_state[partId];
	}

	g_seeds[g_id] = MWC64X_NextUint(&s);
}
