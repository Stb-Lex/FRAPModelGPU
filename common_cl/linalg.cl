#ifndef LINALG_CL
#define LINALG_CL

float4 dot22(const float4 A, const float4 B)
{
	return (float4)(A.x*B.x+A.y*B.z, A.x*B.y+A.y*B.w, A.z*B.x+A.w*B.z, A.z*B.y+A.w*B.w);
}

float2 dot22vec(const float4 A, const float2 V)
{
	return (float2)(A.x*V.x+A.y*V.y, A.z*V.x+A.w*V.y);
}

float4 inv22(const float4 A)
{
	return 1.f/(A.x*A.w-A.y*A.z)*(float4)(A.w, -A.y, -A.z, A.x);
}

float eig22(const float4 A, float2* v1, float2* v2, float2* eig)
{
	float delta = (A.x+A.w)*(A.x+A.w)-4.f*(A.x*A.w-A.y*A.z);
	(*eig).x = ((A.x+A.w)+sqrt(delta))/2.f;
	(*eig).y = ((A.x+A.w)-sqrt(delta))/2.f;

	(*v1).x = (A.x-A.w+sqrt(delta))/2.f/A.z;
	(*v1).y = 1.f;

	(*v2).x = (A.x-A.w-sqrt(delta))/2.f/A.z;
	(*v2).y = 1.f;

	return delta;
}

float eig22mat(const float4 A, float4* v, float2* eig)
{
	float delta = (A.x+A.w)*(A.x+A.w)-4.f*(A.x*A.w-A.y*A.z);
	(*eig).x = ((A.x+A.w)+sqrt(delta))/2.f;
	(*eig).y = ((A.x+A.w)-sqrt(delta))/2.f;

	(*v).x = (A.x-A.w+sqrt(delta))/2.f/A.z;
	(*v).z = 1.f;
	(*v).y = (A.x-A.w-sqrt(delta))/2.f/A.z;
	(*v).w = 1.f;

	return delta;
}

#endif // LINALG_CL
