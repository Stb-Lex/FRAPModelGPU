#include"sample.cl"
#include"state.cl"

bool validateSample(mwc64x_state_t* s, int2 pixelPos, float maskValue)
{
	if(pixelPos.x < 0 || pixelPos.x >= (int)DIMX || pixelPos.y < 0 || pixelPos.y >= (int)DIMY)
		 return false;

	if(sampleUniform(s) <= maskValue)
		return true;

	return false;
}

__kernel void init_mask(__global uint* g_seeds, __global float2* g_pos, __global uchar* g_state, __global const float* g_mask)
{
	size_t g_id = get_global_id(0);

	mwc64x_state_t s;
	MWC64X_SeedStreams(&s, g_seeds[g_id], 0);
	
	for(uint i = 0; i < PARTICLESPERWORKER; i++)
	{
		bool valid = false;
		uint partId = g_id*PARTICLESPERWORKER+i;
		
		while(!valid)
		{
			float2 pos = (float2)((float)DIMX*sampleUniform(&s), (float)DIMY*sampleUniform(&s));
			int2 pixelPos = convert_int2_rtz(pos);

			valid = validateSample(&s, pixelPos, g_mask[pixelPos.y*DIMX+pixelPos.x]);

			if(valid)
			{
				g_pos[partId] = pos;
				g_state[partId] = 0;
				if(sampleUniform(&s) <= (float)PIOFF)
					g_state[partId] = g_state[partId] | STATE_BOUNDED;
			}
		}
	}
	
	g_seeds[g_id] = MWC64X_NextUint(&s);
}
