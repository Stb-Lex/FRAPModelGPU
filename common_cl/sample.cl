#ifndef SAMPLE_CL
#define SAMPLE_CL

#include"mwc64x.cl"

/*
inline ulong xorshift128plus(__local ulong2* s)
{
	ulong x = (*s).x;
	ulong const y = (*s).y;
	(*s).x = y;
	x ^= x << 23;
	(*s).y = x ^ y ^ (x >> 17) ^ (y >> 26);
	return (*s).y + y;
}


inline float sampleUniform_old(__local ulong2* s)
{
	return (float)xorshift128plus(s)/0xffffffffffffffff;
}


inline float sampleNormal_old(__local ulong2* s, float mu, float sigma)
{
	return sigma*sqrt(-2.f*log(sampleUniform(s)))*cos(2.f*M_PI*sampleUniform(s)) + mu;
}
*/

float sampleUniform(mwc64x_state_t* s)
{
	return (float)MWC64X_NextUint(s)/0xFFFFFFFF;
}


float sampleStandardNormal(mwc64x_state_t* s)
{
	float2 u = (float2)(sampleUniform(s), sampleUniform(s));
	while(u.x <= FLT_MIN)
		u.x = sampleUniform(s);
		       
	#ifdef __FAST_RELAXED_MATH__
	return native_sqrt(-2.f*native_log(u.x))*native_cos(2.f*M_PI*u.y);
	#else
	return sqrt(-2.f*log(u.x))*cos(2.f*M_PI*u.y);
	#endif
}

float2 sampleStandardNormal2(mwc64x_state_t* s)
{
	float2 u = (float2)(sampleUniform(s), sampleUniform(s));
	while(u.x <= FLT_MIN)
		u.x = sampleUniform(s);

	#ifdef __FAST_RELAXED_MATH__
	return (float2)(native_sqrt(-2.f*native_log(u.x))*native_cos(2.f*M_PI*u.y), native_sqrt(-2.f*native_log(u.x))*native_sin(2.f*M_PI*u.y));
	#else
	return (float2)(sqrt(-2.f*log(u.x))*cos(2.f*M_PI*u.y), sqrt(-2.f*log(u.x))*sin(2.f*M_PI*u.y));
	#endif
}

#endif // SAMPLE_CL