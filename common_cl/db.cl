#include"spectral_simulate.cl"


__kernel void db(__global const float* g_C0fftr, __global const float* g_C0ffti,
	 __global float* g_Ctfftr, __global float* g_Ctffti)
{
	uint2 g_id = (uint2)(get_global_id(0), get_global_id(1));
	uint2 g_dim = (uint2)(get_global_size(0), get_global_size(1));

	uint posIn = g_id.y*g_dim.x+g_id.x;

	uint2 pos = (uint2)select(g_dim-g_id, g_id, g_id-g_dim/2);
	float2 posf = 2.f*(float)M_PI*convert_float2(pos)/convert_float2(g_dim);
	float posfsq = posf.x*posf.x+posf.y*posf.y;

	float2 pi = (float2)(KOFF, KON)/(float)(KON+KOFF);

	for(uint i = 0; i < FRAMECOUNT; i++)
	{
		uint posOut = i*g_dim.x*g_dim.y+posIn;

		float t = (i+1)*DT;

		float2 Ctr = spectralDiffuse(posfsq, t, g_C0fftr[posIn]*pi);
		float2 Cti = spectralDiffuse(posfsq, t, g_C0ffti[posIn]*pi);

		g_Ctfftr[posOut] = Ctr.x + Ctr.y;
		g_Ctffti[posOut] = Cti.x + Cti.y;
	}
}
