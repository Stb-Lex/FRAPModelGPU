#include"state.cl"

__kernel void bin2d(__global const float2* g_pts, __global const uchar* g_state, __global int* g_bin, const uint iter)
{
	size_t g_id = get_global_id(0);

	for(uint i = 0; i < PARTICLESPERWORKER; i++)
	{
		uint partId = PARTICLESPERWORKER*g_id+i;
		
		if(!(g_state[partId] & STATE_INACTIVE))
		{
			int2 iPts = convert_int2_rtz(g_pts[PARTICLESPERWORKER*g_id+i]);
			if(iPts.x < DIMX && iPts.x >= 0 && iPts.y < DIMY && iPts.y >= 0)
				atomic_inc(&g_bin[iter*DIMX*DIMY+iPts.y*DIMX+iPts.x]);
		}
	}
}
