__kernel void to_zoomed_space(__global float2* g_pos)
{
	uint id = get_global_id(0);

	float2 translate = ((float)ZOOM-1.f)/2.f*(float2)((float)DIMX, (float)DIMY);

	for(uint i = 0; i < PARTICLESPERWORKER; i++)
		g_pos[id*PARTICLESPERWORKER+i] = ((float)ZOOM)*(g_pos[id*PARTICLESPERWORKER+i])-translate;
}


__kernel void to_unzoomed_space(__global float2* g_pos)
{
	uint id = get_global_id(0);

	float2 translate = ((float)ZOOM-1.f)/2.f*(float2)((float)DIMX, (float)DIMY);

	for(uint i = 0; i < PARTICLESPERWORKER; i++)
		g_pos[id*PARTICLESPERWORKER+i] = (g_pos[id*PARTICLESPERWORKER+i]+translate)/((float)ZOOM);
}

