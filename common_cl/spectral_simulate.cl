#ifndef SPECTRAL_SIMULATE_CL
#define SPECTRAL_SIMULATE_CL

#include"linalg.cl"


#ifdef DB

float2 spectralDiffuse(float kSquared, float t, float2 C0)
{
	float4 A = (float4)(-kSquared*D-KON, KOFF, KON, -KOFF);

	float4 v;
	float2 eig;

	eig22mat(A, &v, &eig);

	float4 vinv = inv22(v);

	float4 Lambda = (float4)(exp(eig.x*t), 0, 0, exp(eig.y*t));

	float4 expA = dot22(dot22(v, Lambda), vinv);

	return dot22vec(expA, C0);
}

#else

float2 spectralDiffuse(float kSquared, float t, float2 C0)
{
	return exp(-kSquared*(float)D*t)*C0;
}

#endif

#endif // SPECTRAL_SIMULATE_CL
