void __kernel init_gaussian(__global float* c0)
{
	uint id = get_global_id(1)*get_global_size(0) + get_global_id(0);

	float2 pos = (float2)((float)get_global_id(0) - 0.5f*get_global_size(0) + 0.5f, (float)get_global_id(1) - 0.5f*get_global_size(1) + 0.5f);

	c0[id] = IU - (IU-IB)*exp(-(pos.x*pos.x+pos.y*pos.y)/RADIUS/RADIUS/2);
}
