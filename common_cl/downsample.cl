__kernel void downsample(const __global float* g_scaled, __global float* g_out)
{
	uint2 g_pos = (uint2)(get_global_id(0), get_global_id(1));
	
	uint g_id = g_pos.y*get_global_size(0) + get_global_id(0);

	float m = 0.f;
	for(unsigned int i = 0; i < KERNEL_SIZE; i++)
		for(unsigned int j = 0; j < KERNEL_SIZE; j++)
			m += g_scaled[(g_pos.x+g_pos.y*KERNEL_SIZE*get_global_size(0))*KERNEL_SIZE+i*KERNEL_SIZE*get_global_size(0)+j];

	g_out[g_id] = m/KERNEL_SIZE/KERNEL_SIZE;
}
