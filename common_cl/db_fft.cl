#include"linalg.cl"

float2 diffuse(const float posfsq, const float t, const float2 Ct)
{
	float4 A = (float4)(-posfsq*D-KON, KOFF, KON, -KOFF);

	float4 v;
	float2 eig;

	eig22mat(A, &v, &eig);

	float4 vinv = inv22(v);

	float4 Lambda = (float4)(exp(eig.x*t), 0, 0, exp(eig.y*t));

	float4 expA = dot22(dot22(v, Lambda), vinv);

	return dot22vec(expA, Ct);
}

__kernel void db_fft(__global const float* U0fftr, __global const float* U0ffti,
	 __global const float* B0fftr, __global const float* B0ffti,
	 __global float* Ctfftr, __global float* Ctffti)	 
{
	uint2 id = (uint2)(get_global_id(0), get_global_id(1));
	uint2 dim = (uint2)(get_global_size(0), get_global_size(1));

	uint posIn = id.y*dim.x+id.x;

	uint2 pos = (uint2)select(dim-id, id, id-dim/2);
	float2 posf = 2.f*(float)M_PI*convert_float2(pos)/convert_float2(dim);
	float posfsq = posf.x*posf.x+posf.y*posf.y;

	float2 C0r = (float2)(U0fftr[posIn], B0fftr[posIn]);
	float2 C0i = (float2)(U0ffti[posIn], B0ffti[posIn]);

	for(uint i = 0; i < FRAMECOUNT; i++)
	{
		float t = (i+1)*DT;

		float2 Ctr = diffuse(posfsq, t, C0r);
		float2 Cti = diffuse(posfsq, t, C0i);

		uint posOut = i*dim.x*dim.y+posIn;

		Ctfftr[posOut] = Ctr.x + Ctr.y;
		Ctffti[posOut] = Cti.x + Cti.y;
	}
}
