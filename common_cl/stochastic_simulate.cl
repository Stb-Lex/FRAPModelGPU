#ifndef STOCHASTIC_SIMULATE
#define STOCHASTIC_SIMULATE


#include"state.cl"
#include"sample.cl"

#ifdef DB

void stochasticDiffuse(mwc64x_state_t* s, __private float2* pos, __private uchar* state, float dt)
{
	#pragma unroll 1
	for(int j = 0; j < SIMSTEPS; j++)
	{
		// We first check if the particle changes state
		if(!((*state) & STATE_BOUNDED))
			(*state) = sampleUniform(s) <= dt/(float)MUON ? (*state) | STATE_BOUNDED : (*state);
		else
			(*state) = sampleUniform(s) <= dt/(float)MUOFF ? (*state) & ~STATE_BOUNDED : (*state);
		
		// If the particle is unbound, then we diffuse it
		if(!((*state) & STATE_BOUNDED))
		{
			#ifdef ZOOM
			
			#ifdef __FAST_RELAXED_MATH__
			float stdev = native_sqrt(2.f*(float)ZOOM*(float)D*dt);
			#else
			float stdev = sqrt(2.f*(float)ZOOM*(float)D*dt);
			#endif	

			(*pos) += stdev*sampleStandardNormal2(s);
			
			float2 dimPos = ((float)ZOOM+1.f)/2.f*(float2)((float)DIMX, (float)DIMY);
			float2 dimNeg = -((float)ZOOM-1.f)/2.f*(float2)((float)DIMX, (float)DIMY);

			(*pos) = select((*pos), (*pos) + (float)ZOOM*(float2)((float)DIMX, (float)DIMY), (*pos) < dimNeg);
			(*pos) = select((*pos), (*pos) - (float)ZOOM*(float2)((float)DIMX, (float)DIMY), (*pos) >= dimPos);
			#else
			
			#ifdef __FAST_RELAXED_MATH__
			float stdev = native_sqrt(2.f*(float)D*dt);
			#else
			float stdev = sqrt(2.f*(float)D*dt);
			#endif

			(*pos) += stdev*sampleStandardNormal2(s);

			(*pos) = select((*pos), (*pos) + (float2)((float)DIMX, (float)DIMY), (*pos) < (float2)(0.f, 0.f));
			(*pos) = select((*pos), (*pos) - (float2)((float)DIMX, (float)DIMY), (*pos) >= (float2)((float)DIMX, (float)DIMY));
			#endif
		}
	}

}

#else

void stochasticDiffuse(mwc64x_state_t* s, __private float2* pos, __private uchar* state, float dt)
{
	#pragma unroll 1
	for(int j = 0; j < SIMSTEPS; j++)
	{
		#ifdef ZOOM
		#ifdef __FAST_RELAXED_MATH__
		float stdev = native_sqrt(2.f*(float)ZOOM*(float)D*dt);
		#else
		float stdev = sqrt(2.f*(float)ZOOM*(float)D*dt);
		#endif

		(*pos) += stdev*sampleStandardNormal2(s);

		float2 dimPos = ((float)ZOOM+1.f)/2.f*(float2)((float)DIMX, (float)DIMY);
		float2 dimNeg = -((float)ZOOM-1.f)/2.f*(float2)((float)DIMX, (float)DIMY);

		(*pos) = select((*pos), (*pos) + (float)ZOOM*(float2)((float)DIMX, (float)DIMY), (*pos) < dimNeg);
		(*pos) = select((*pos), (*pos) - (float)ZOOM*(float2)((float)DIMX, (float)DIMY), (*pos) >= dimPos);
		#else
		(*pos) = select((*pos), (*pos) + (float2)((float)DIMX, (float)DIMY), (*pos) < (float2)(0.f, 0.f));
		(*pos) = select((*pos), (*pos) - (float2)((float)DIMX, (float)DIMY), (*pos) >= (float2)((float)DIMX, (float)DIMY));
		#endif
	}
}

#endif

void simulateAllParticles_l(mwc64x_state_t* s, __local float2* l_pos, __local uchar* l_state, float dt)
{
	for(uint i = 0; i < PARTICLESPERWORKER; i++)
	{
		float2 pos = l_pos[i];
		uchar state = l_state[i];

		stochasticDiffuse(s, &pos, &state, dt);

		l_pos[i] = pos;
		l_state[i] = state;
	}
}


void simulateAllParticles_g(mwc64x_state_t* s, __global float2* g_pos, __global uchar* g_state, float dt)
{
	for(uint i = 0; i < PARTICLESPERWORKER; i++)
	{
		float2 pos = g_pos[i];
		uchar state = g_state[i];
	
		stochasticDiffuse(s, &pos, &state, dt);
			
		g_pos[i] = pos;
		g_state[i] = state;
	}
}

#endif // STOCHASTIC_SIMULATE
