// It's called "spectral_bleach.cl" but the bleaching is done in space domain

__kernel void bleach(__global float* g_U, __global float* g_B, __global const float* g_bleachMask)
{
	uint g_id = get_global_id(1)*get_global_size(0)+get_global_id(0);

	float bleachValue = g_bleachMask[g_id];

	g_U[g_id] *= bleachValue;
	g_B[g_id] *= bleachValue;
}
