#include"sample.cl"
#include"state.cl"

bool validateSample(float2 pos)
{
	if(pos.x < 0.f || pos.x >= (float)DIMX || pos.y < 0.f || pos.y >= (float)DIMY)
		return false;
	
	return true;
}

__kernel void init_homogeneous(__global uint* g_seeds, __global float2* g_pos, __global uchar* g_state)
{
	uint id = get_global_id(0);

	mwc64x_state_t s;
	MWC64X_SeedStreams(&s, g_seeds[id], 0);

	for(uint i = 0; i < PARTICLESPERWORKER; i++)
	{
		uint partId = PARTICLESPERWORKER*id+i;

		bool valid = false;

		float2 pos;

		while(!valid)
		{
			pos = (float2)((float)DIMX*sampleUniform(&s), (float)DIMY*sampleUniform(&s));

			valid = validateSample(pos);

			if(valid)
			{
				g_state[partId] = 0;
				g_pos[partId] = pos;
				if(sampleUniform(&s) <= (float)PIOFF)
					g_state[partId] = g_state[partId] | STATE_BOUNDED;
			}
		}
	}

	g_seeds[id] = MWC64X_NextUint(&s);
}
