#include"stochastic_simulate.cl"


__kernel void stochastic_scan_bleach(__global uint* g_seed, __global float2* g_pos, __global uchar* g_state, __global float* g_bleachMask, __local float2* l_pos, __local uchar* l_state)
{
	uint g_id = get_global_id(0);
	uint l_id = get_local_id(0);

	mwc64x_state_t s;
	MWC64X_SeedStreams(&s, g_seed[g_id], 0);

	for(uint i = 0; i < PARTICLESPERWORKER; i++)
	{
		l_pos[l_id*PARTICLESPERWORKER+i] = g_pos[g_id*PARTICLESPERWORKER+i];
		l_state[l_id*PARTICLESPERWORKER+i] = g_state[g_id*PARTICLESPERWORKER+i];
	}

	for(uint i = 0; i < DIMY; i++)
	{
		for(uint j = 0; j < DIMX; j += PIXELSTRIDE)
		{
			simulateAllParticles_l(&s, &l_pos[l_id*PARTICLESPERWORKER], &l_state[l_id*PARTICLESPERWORKER], PIXELSTRIDE*PDT);

			for(uint k = 0; k < PIXELSTRIDE; k++)
			{
				int xPos = j+k;
				float bleachMaskValue = g_bleachMask[i*DIMX+xPos];

				for(uint l = 0; l < PARTICLESPERWORKER; l++)
				{
					uint l_partId = l_id*PARTICLESPERWORKER+l;
					int2 pixelPos = convert_int2_rtz(l_pos[l_partId]);
					
					if(pixelPos.x == xPos && pixelPos.y == i)
						if(sampleUniform(&s) > bleachMaskValue)
							l_state[l_partId] = l_state[l_partId] | STATE_INACTIVE;


				}
			}
		}

		simulateAllParticles_l(&s, &l_pos[l_id*PARTICLESPERWORKER], &l_state[l_id*PARTICLESPERWORKER], NLT);
	}

	simulateAllParticles_l(&s, &l_pos[l_id*PARTICLESPERWORKER], &l_state[l_id*PARTICLESPERWORKER], BTOT);

	for(uint i = 0; i < PARTICLESPERWORKER; i++)
	{
		g_pos[g_id*PARTICLESPERWORKER+i] = l_pos[l_id*PARTICLESPERWORKER+i];
		g_state[g_id*PARTICLESPERWORKER+i] = l_state[l_id*PARTICLESPERWORKER+i];
	}

	g_seed[g_id] = MWC64X_NextUint(&s);
}
