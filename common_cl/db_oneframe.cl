#include"spectral_simulate.cl"


__kernel void db_oneframe(__global float* g_Ufftr, __global float* g_Uffti, __global float* g_Bfftr, __global float* g_Bffti, __global float* g_Cfftr, __global float* g_Cffti)
{
	uint2 g_id = (uint2)(get_global_id(0), get_global_id(1));
	uint2 g_dim = (uint2)(get_global_size(0), get_global_size(1));

	uint posId = g_id.y*g_dim.x+g_id.x;

	uint2 pos = (uint2)select(g_dim-g_id, g_id, g_id-g_dim/2);
	float2 posf = 2.f*(float)M_PI*convert_float2(pos)/convert_float2(g_dim);
	float posfsq = posf.x*posf.x+posf.y*posf.y;


	float2 Ctr = spectralDiffuse(posfsq, DT, (float2)(g_Ufftr[posId], g_Bfftr[posId]));
	float2 Cti = spectralDiffuse(posfsq, DT, (float2)(g_Uffti[posId], g_Bffti[posId]));

	g_Ufftr[posId] = Ctr.x;
	g_Uffti[posId] = Cti.x;
	g_Bfftr[posId] = Ctr.y;
	g_Bffti[posId] = Cti.y;

	g_Cfftr[posId] = Ctr.x + Ctr.y;
	g_Cffti[posId] = Cti.x + Cti.y;
}
