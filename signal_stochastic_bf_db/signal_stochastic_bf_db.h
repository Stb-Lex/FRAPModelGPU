#ifndef SIGNAL_STOCHASTIC_BF_DB_H
#define SIGNAL_STOCHASTIC_BF_DB_H

#include<vector>
#include<iostream>

struct simulation_ssbfdb_param_t
{
  unsigned int N;
  unsigned int padding;
  size_t dimxy[2];
  unsigned int frames;
  unsigned int bleachFrames;
  
  unsigned int workers;
  unsigned int particlesPerWorker;
  unsigned int Nparticles;
  
  float bleachCoef;
  float readBleachCoef;
  
  unsigned int simSteps;
  float dT;

  bool db;
  float k[2];
  float mu[2];
  float pi[2];
  float D;

  std::string bleachType;
  float bleachRadius;
  float bleachRect[2];

  bool seeded;
  unsigned int seed;
};

std::ostream& operator<<(std::ostream& stream, const simulation_ssbfdb_param_t& param);

bool loadSimulationParamFromConfigFile(simulation_ssbfdb_param_t& param, const char* szConfigFilename);

#endif // SIGNAL_STOCHASTIC_BF_DB_H
