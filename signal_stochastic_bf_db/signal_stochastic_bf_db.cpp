#include"signal_stochastic_bf_db.h"
#include<libconfig.h++>
#include<ctime>


std::ostream& operator<<(std::ostream& stream, const simulation_ssbfdb_param_t& param)
{
  std::cout << "Simulation parameters:" << std::endl;
  std::cout << "Resolution N = " << param.N << ", padding = " << param.padding << std::endl;
  std::cout << "Effective resolution = [" << param.dimxy[0] << ", " << param.dimxy[1] << "]" << std::endl;
  std::cout << "Number of bleach frames = " << param.bleachFrames << ", number of frames = " << param.frames << std::endl;
  std::cout << "Number of workers = " << param.workers << ", number of particles per worker = " << param.particlesPerWorker << ", total numbers of particles = " << param.Nparticles << std::endl;
  std::cout << "dT = " << param.dT << ", simulation steps = " << param.simSteps << std::endl;
  std::cout << "Diffusion-binding simulation: " << param.db << std::endl;
  if(param.db)
  {
    std::cout << "Kon = " << param.k[0] << ", koff = " << param.k[1] << std::endl;
    std::cout << "muon = " << param.mu[0] << ", muoff = " << param.mu[1] << std::endl;
    std::cout << "pion = " << param.pi[0] << ", pioff = " << param.pi[1] << std::endl;
  }
  std::cout << "Diffusion constant D = " << param.D << std::endl;
  std::cout << "Bleach coefficient = " << param.bleachCoef << std::endl;
  std::cout << "Bleach spot shape: " << param.bleachType << std::endl;
  if(param.bleachType == "disk" || param.bleachType == "gaussian")
    std::cout << "Radius = " << param.bleachRadius << std::endl;
  else if(param.bleachType == "rect")
    std::cout << "Rectangle dimension = [" << param.bleachRect[0] << ", " << param.bleachRect[1] << "]" << std::endl;
  std::cout << "Seed = " << param.seed;
}


bool loadSimulationParamFromConfigFile(simulation_ssbfdb_param_t& param, const char* configFilename)
{
  libconfig::Config cfg;

  try
  {
    cfg.readFile(configFilename);
  }
  catch(libconfig::ParseException& e)
  {
    std::cout << "Error when parsing simulation config file " << configFilename << " at line " << e.getLine() << std::endl;
    std::cout << e.getError() << std::endl;
    return false;
  }
  catch(libconfig::FileIOException& e)
  {
    std::cout << "Could not open simulation config file " << configFilename << std::endl;
    return false;
  }

  try
  {
    cfg.lookup("simulation.N");
    cfg.lookup("simulation.padding");
    cfg.lookup("simulation.frames");
    cfg.lookup("simulation.bleachFrames");
    cfg.lookup("simulation.workers");
    cfg.lookup("simulation.particlesPerWorker");
    cfg.lookup("simulation.D");
    cfg.lookup("simulation.simSteps");
    cfg.lookup("simulation.dT");
    cfg.lookup("simulation.kon");
    cfg.lookup("simulation.koff");
    cfg.lookup("simulation.bleachType");
    cfg.lookup("simulation.bleachCoef");
    cfg.lookup("simulation.db");
    cfg.lookup("simulation.seeded");
    
    std::string bleachType;
    cfg.lookupValue("simulation.bleachType", bleachType);
    if(bleachType == "disk" || bleachType == "gaussian")
      cfg.lookup("simulation.bleachRadius");
    else if(bleachType == "rect")
    {
      const libconfig::Setting& bleachRect = cfg.lookup("simulation.bleachRect");
      if(bleachRect.getLength() != 2)
      {
	std::cout << "Badly formed array for bleachRect!" << std::endl;
	return false;
      }
    }
    else if(bleachType == "none");
    else
    {
      std::cerr << "Could not recognise bleach type: " << bleachType << std::endl;
      return false;
    }
  }
  catch(libconfig::SettingNotFoundException& e)
  {
    std::cout << "Error when loading simulation parameters from config file! Could not find: " << e.getPath() << std::endl;
    return false;
  }
  
  cfg.lookupValue("simulation.N", param.N);
  cfg.lookupValue("simulation.padding", param.padding);
  param.dimxy[0] = param.N+2*param.padding;
  param.dimxy[1] = param.N+2*param.padding;
  cfg.lookupValue("simulation.frames", param.frames);
  cfg.lookupValue("simulation.bleachFrames", param.bleachFrames);
  cfg.lookupValue("simulation.workers", param.workers);
  cfg.lookupValue("simulation.particlesPerWorker", param.particlesPerWorker);
  param.Nparticles = param.workers*param.particlesPerWorker;
  cfg.lookupValue("simulation.bleachCoef", param.bleachCoef);
  cfg.lookupValue("simulation.simSteps", param.simSteps);
  cfg.lookupValue("simulation.dT", param.dT);
  cfg.lookupValue("simulation.kon", param.k[0]);
  cfg.lookupValue("simulation.koff", param.k[1]);
  param.mu[0] = 1.f/param.k[0];
  param.mu[1] = 1.f/param.k[1];
  param.pi[0] = param.k[1]/(param.k[0]+param.k[1]);
  param.pi[1] = param.k[0]/(param.k[0]+param.k[1]);
  cfg.lookupValue("simulation.D", param.D);
  cfg.lookupValue("simulation.db", param.db);
  cfg.lookupValue("simulation.seeded", param.seeded);

  if(param.seeded)
  {
    if(!cfg.lookupValue("simulation.seed", param.seed))
    {
      std::cout << "Warning! Simulation seed should be provided! Generated instead." << std::endl;
      param.seed = std::time(nullptr);
    }
  }
  else
    param.seed = time(nullptr);

  cfg.lookupValue("simulation.bleachType", param.bleachType);
  if(param.bleachType == "disk" || param.bleachType == "gaussian")
  {
    cfg.lookupValue("simulation.bleachRadius", param.bleachRadius);
  }
  else if(param.bleachType == "rect")
  {
    const libconfig::Setting& bleachRect = cfg.lookup("simulation.bleachRect");
    param.bleachRect[0] = bleachRect[0];
    param.bleachRect[1] = bleachRect[1];
  }

  return true;
}
