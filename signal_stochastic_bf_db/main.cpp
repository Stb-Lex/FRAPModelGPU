#include<CL/opencl.h>
#include<iostream>
#include<string>
#include<Eigen/Dense>
#include<cmath>

#include"timer.h"
#include"opencl_common.h"
#include"signal_stochastic_db.h"
#include"mask.h"
#include"export_solution.h"
#include"signal_stochastic_bf_db.h"
#include"particles_common.h"
#include"initial_sdb.h"

#define DEBUG_BLEACH_FRAMES
//#define DEBUG_DUMP_PARTICLES

#define WITH_CONSOLIDATION

int main(int argc, char* argv[])
{
  putenv("CUDA_CACHE_DISABLE=1");

  if(argc != 4)
  {
    std::cout << "Usage: signal_stochastic_bf_db device_config_filename simulation_config_filename output_filename" << std::endl;
    return 0;
  }  

  const char* szDeviceConfigFilename = argv[1];
  const char* szSimulationConfigFilename = argv[2];
  std::string szOutputFilename = argv[3];
  
  // OpenCL related vars  
  cl_platform_id platformId;
  cl_device_id deviceId;
  cl_context context;
  cl_command_queue commandQueue;
  cl_int clerror;

  Timer myPerfTimer;
  std::cout << "Initialising OpenCL from " << szDeviceConfigFilename << "... " << std::flush;

  if(initOpenCLByConfigFile(platformId, context, deviceId, commandQueue, argv[1]) != CL_SUCCESS)
    return -1;

  std::cout << "Done. Elapsed time: " << myPerfTimer.Elapsed() << "s" << std::endl;

  std::cout << "Loading simulation parameters from " << szSimulationConfigFilename << "... " << std::flush;
  simulation_ssbfdb_param_t param;
  if(!loadSimulationParamFromConfigFile(param, szSimulationConfigFilename))
    return -1;
  std::cout << "Done" << std::endl;

  std::cout << param << std::endl;

  std::cout << "Output file: " << szOutputFilename << std::endl;
  
  cl_mem bufSeeds = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(cl_uint)*param.workers, nullptr, nullptr);
  cl_mem bufPos = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(cl_float2)*param.Nparticles, nullptr, nullptr);
  cl_mem bufState = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(cl_uchar)*param.Nparticles, nullptr, nullptr);

  std::cout << "Seeding... " << std::flush;

  cl_uint* pSeeds = new cl_uint[param.workers];

  srand(param.seed);

  // We initialise the seed buffer
  for(unsigned int i = 0; i < param.workers; i++)
    pSeeds[i] = rand();

  OCLCheck(clEnqueueWriteBuffer(commandQueue, bufSeeds, CL_TRUE, 0, sizeof(cl_uint)*param.workers, pSeeds, 0, nullptr, nullptr));

  delete[] pSeeds;

  std::cout << "Done" << std::endl;

  std::cout << "Initial conditions..." << std::flush;
  myPerfTimer = Timer();

  if(init_homogeneous(context, commandQueue, deviceId, bufSeeds, bufPos, bufState, param.workers, param.particlesPerWorker, param.pi, param.dimxy) != CL_SUCCESS)
    return -1;

  std::cout << "Done. Elapsed time: " << myPerfTimer.Elapsed() << "s" << std::endl;
  
  std::cout << "Bleaching..." << std::endl;
  myPerfTimer = Timer();
  

  #ifdef DEBUG_BLEACH_FRAMES
  std::vector<Eigen::ArrayXXi> bleachFrames;
  #endif

  // We now bleach bleachFrame time
  for(unsigned int i = 0; i < param.bleachFrames; i++)
  {
    std::cout << "Bleaching frame " << i << "... " << std::endl;

    Timer timerBleaching;
      // Because of a bug with the Nvidia driver, we have to create/release the mask buffer for each frame. Investigation is needed in order to find a better solution.

    cl_mem bufBleachProfile = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(cl_float)*param.dimxy[0]*param.dimxy[1], nullptr, nullptr);
    // We fill the bleach profile mask
  if(param.bleachType == "disk")
    clerror = create_mask_disk(context, commandQueue, deviceId, bufBleachProfile, param.dimxy, param.bleachRadius, param.bleachCoef, 1.0f, 2);
  else if(param.bleachType == "gaussian")
    clerror = create_mask_gaussian(context, commandQueue, deviceId, bufBleachProfile, param.dimxy, param.bleachRadius, param.bleachCoef, 1.0f);
  else if(param.bleachType == "rect")
    clerror = create_mask_rect(context, commandQueue, deviceId, bufBleachProfile, param.dimxy, param.bleachRect, param.bleachCoef, 1.0f, 2);
  else
    clerror = create_mask_rect(context, commandQueue, deviceId, bufBleachProfile, param.dimxy, param.bleachRect, 1.f, 1.0f, 1);    

  if(clerror != CL_SUCCESS)
    return -1;

        // Bleaching
    if(bleach_mask(context, commandQueue, deviceId, bufSeeds, bufPos, bufState, bufBleachProfile, param.workers, param.particlesPerWorker, param.dimxy) != CL_SUCCESS)
      return -1;

    clReleaseMemObject(bufBleachProfile);

    std::cout << "Done. Elapsed time: " << timerBleaching.Elapsed() << "s" << std::endl;

    #ifdef DEBUG_BLEACH_FRAMES
    Eigen::ArrayXXi curBleachFrame;
    get_frame_from_particles(context, commandQueue, deviceId, bufPos, bufState, param.workers, param.particlesPerWorker, param.dimxy, curBleachFrame);
    bleachFrames.push_back(curBleachFrame);
    #endif

    #ifdef DEBUG_DUMP_PARTICLES
    std::cout << std::endl << dump_particles_state(commandQueue, bufState, param.particles) << std::endl;
    #endif

    std::cout << "Simulation pass... " << std::flush;
    Timer timerSimulation;
    
    // Simulation
    if(signal_stochastic_db_one_step(context, commandQueue, deviceId, bufSeeds, bufPos, bufState, param.workers, param.particlesPerWorker, param.simSteps, param.D, param.dT, param.mu, param.db, param.dimxy) != CL_SUCCESS)
      return -1;

    std::cout << "Done. Elapsed time: " << timerSimulation.Elapsed() << "s" << std::endl;
  }
  
  #ifdef DEBUG_BLEACH_FRAMES
  export_csv<Eigen::ArrayXXi>("bleach_" + szOutputFilename, bleachFrames);
  #endif

  std::cout << "Done bleaching. Time elapsed: " << myPerfTimer.Elapsed() << "s" << std::endl;

  
#ifdef WITH_CONSOLIDATION
  std::cout << "Consolidation pass... " << std::flush;
  Timer timerConsolidation;

  unsigned int oldParticlesPerWorker = param.particlesPerWorker;
  param.particlesPerWorker = consolidate_active_particles(commandQueue, bufPos, bufState, param.workers, param.particlesPerWorker);

  std::cout << "Done. Elapsed time: " << timerConsolidation.Elapsed() << "s" << std::endl;
  std::cout << "New particlesPerWorker = " << param.particlesPerWorker << " (lost " << (1.f-(float)param.particlesPerWorker/oldParticlesPerWorker)*100.f << "%)" << std::endl;
    #endif


  std::vector<Eigen::ArrayXXi> Ct;

  if(signal_stochastic_db(context, commandQueue, deviceId, bufSeeds, bufPos, bufState, param.workers, param.particlesPerWorker, param.simSteps, param.D, param.dT, param.mu, param.db, param.dimxy, param.frames, Ct) != CL_SUCCESS)
    return -1;

  std::cout << "Saving result to " << szOutputFilename << ". " << std::flush;
  export_csv<Eigen::ArrayXXi>(szOutputFilename, Ct);
  std::cout << "Done" << std::endl;

  std::cout << "Cleaning..." << std::flush;
  clReleaseMemObject(bufSeeds);
  clReleaseMemObject(bufPos);
  clReleaseMemObject(bufState);
  clReleaseCommandQueue(commandQueue);
  clReleaseContext(context);
  std::cout << "Done" << std::endl;

  return 0;

}
