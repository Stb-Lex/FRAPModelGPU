#include"sample.cl"
#include"state.cl"
#include"stochastic_simulate.cl"

__kernel void stochastic_db(__global uint* g_seeds, __global float2* g_pos, __global uchar* g_state)
{
	uint g_id = get_global_id(0);
	
	mwc64x_state_t s;
	MWC64X_SeedStreams(&s, g_seeds[g_id], 0);

	simulateAllParticles_g(&s, &g_pos[g_id*PARTICLESPERWORKER], &g_state[g_id*PARTICLESPERWORKER], DTSIM);

	g_seeds[g_id] = MWC64X_NextUint(&s);
}
