#include<CL/opencl.h>
#include<iostream>
#include<Eigen/Dense>
#include"timer.h"
#include"opencl_common.h"
#include"signal_stochastic_db.h"
#include"initial_sdb.h"
#include"export_solution.h"


int main(int argc, char* argv[])
{
  putenv("CUDA_CACHE_DISABLE=1");
  
  if(argc == 1)
  {
    std::cout << "Usage: signal_stochastic_db device_config_filename simulation_config_filename output_filename" << std::endl;
    return 0;
  }

  const char* szDeviceConfigFilename = argv[1];
  const char* szSimulationConfigFilename = argv[2];
  std::string szOutputFilename = argv[3];
  
  // OpenCL related vars  
  cl_platform_id platformId;
  cl_device_id deviceId;
  cl_context context;
  cl_command_queue commandQueue;
  cl_int clerror;

  Timer myPerfTimer;
  std::cout << "Initialising OpenCL from " << szDeviceConfigFilename << "... " << std::flush;

  if(initOpenCLByConfigFile(platformId, context, deviceId, commandQueue, argv[1]) != CL_SUCCESS)
    return -1;

  std::cout << "Done. Elapsed time: " << myPerfTimer.Elapsed() << "s" << std::endl;

  std::cout << "Loading simulation parameters from " << szSimulationConfigFilename << "... " << std::flush;
  simulation_ssdb_param_t param;
  if(!loadSimulationParamFromConfigFile(param, szSimulationConfigFilename))
    return -1;
  std::cout << "Done" << std::endl;

  std::cout << param << std::endl;

  std::cout << "Output file: " << szOutputFilename << std::endl;

  cl_mem bufSeeds = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(cl_uint)*param.workers, nullptr, nullptr);
  cl_mem bufPos = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(cl_float2)*param.Nparticles, nullptr, nullptr);
  cl_mem bufState = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(cl_uchar)*param.Nparticles, nullptr, nullptr);

  std::cout << "Seeding... " << std::flush;

  cl_uint* pSeeds = new cl_uint[param.workers];

  srand(param.seed);

  // We initialise the seed buffer
  for(unsigned int i = 0; i < param.workers; i++)
    pSeeds[i] = rand();

  clEnqueueWriteBuffer(commandQueue, bufSeeds, CL_TRUE, 0, sizeof(cl_uint)*param.workers, static_cast<const void*>(pSeeds), 0, nullptr, nullptr);

  delete[] pSeeds;

  std::cout << "Done" << std::endl;

  if(param.bleachType == "disk")
    clerror = init_disk(context, commandQueue, deviceId, bufSeeds, bufPos, bufState, param.workers, param.particlesPerWorker, param.pi, param.bleachRadius, param.bleachCoef, param.dimxy);
  else if(param.bleachType == "rect")
    clerror = init_rect(context, commandQueue, deviceId, bufSeeds, bufPos, bufState, param.workers, param.particlesPerWorker, param.pi, param.bleachRect, param.bleachCoef, param.dimxy);
  else if(param.bleachType == "gaussian")
    clerror = init_gaussian(context, commandQueue, deviceId, bufSeeds, bufPos, bufState, param.workers, param.particlesPerWorker, param.pi, param.bleachRadius, param.bleachCoef, param.dimxy);
  else
    clerror = init_homogeneous(context, commandQueue, deviceId, bufSeeds, bufPos, bufState, param.workers, param.particlesPerWorker, param.pi, param.dimxy);

  if(clerror != CL_SUCCESS)
    return -1;

  std::vector<Eigen::ArrayXXi> Ct;

  if(signal_stochastic_db(context, commandQueue, deviceId, bufSeeds, bufPos, bufState, param.workers, param.particlesPerWorker, param.simSteps, param.D, param.dT, param.mu, param.db, param.dimxy, param.frames, Ct) != CL_SUCCESS)
    return -1;

  std::cout << "Saving result to " << szOutputFilename << ". " << std::flush;
  export_csv<Eigen::ArrayXXi>(szOutputFilename, Ct);
  std::cout << "Done" << std::endl;

  std::cout << "Cleaning..." << std::flush;
  clReleaseMemObject(bufSeeds);
  clReleaseMemObject(bufPos);
  clReleaseMemObject(bufState);
  clReleaseCommandQueue(commandQueue);
  clReleaseContext(context);
  std::cout << "Done" << std::endl;

  return 0;

}

