#include"initial_sdb.h"
#include"timer.h"
#include"opencl_common.h"
#include"mask.h"
#include<iostream>


cl_int init_disk(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufSeeds, cl_mem bufPos, cl_mem bufState, unsigned int Nworkers, unsigned int particlesPerWorker, const float pi[2], float radius, float Ib, const size_t dimxy[2])
{
  cl_int clerror;
  unsigned int Nparticles = Nworkers*particlesPerWorker;
  std::cout << "Computing initial conditions for " << Nparticles << " particles..." << std::endl;
  std::cout << "Bleached disk of radius " << radius << std::endl;

  cl_mem bufMask = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(cl_float)*dimxy[0]*dimxy[1], nullptr, nullptr);

  clerror = create_mask_disk(context, commandQueue, deviceId, bufMask, dimxy, radius, Ib, 1.f, 2);

  if(clerror != CL_SUCCESS)
    return clerror;

  clerror = init_mask(context, commandQueue, deviceId, bufSeeds, bufPos, bufState, bufMask, Nworkers, particlesPerWorker, pi, dimxy);

  if(clerror != CL_SUCCESS)
    return clerror;

  OCLCheck(clReleaseMemObject(bufMask));


  return CL_SUCCESS;
}


cl_int init_rect(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufSeeds, cl_mem bufPos, cl_mem bufState, unsigned int Nworkers, unsigned int particlesPerWorker, const float pi[2], const float dimRect[2], float Ib, const size_t dimxy[2])
{
  cl_int clerror;
  unsigned int Nparticles = Nworkers*particlesPerWorker;
  std::cout << "Computing initial conditions for " << Nparticles << " particles..." << std::flush;
  std::cout << "Bleached rectangle of dimension " << dimRect[0] << "x" << dimRect[1] << std::endl;

  cl_mem bufMask = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(cl_float)*dimxy[0]*dimxy[1], nullptr, nullptr);

  clerror = create_mask_rect(context, commandQueue, deviceId, bufMask, dimxy, dimRect, Ib, 1.f, 2);

  if(clerror != CL_SUCCESS)
    return clerror;

  clerror = init_mask(context, commandQueue, deviceId, bufSeeds, bufPos, bufState, bufMask, Nworkers, particlesPerWorker, pi, dimxy);

  if(clerror != CL_SUCCESS)
    return clerror;

  OCLCheck(clReleaseMemObject(bufMask));

  return CL_SUCCESS;
}


cl_int init_gaussian(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufSeeds, cl_mem bufPos, cl_mem bufState, unsigned int Nworkers, unsigned int particlesPerWorker, const float pi[2], float radius, float Ib, const size_t dimxy[2])
{
  cl_int clerror;
  unsigned int Nparticles = Nworkers*particlesPerWorker;
  std::cout << "Computing initial conditions for " << Nparticles << " particles..." << std::endl;
  std::cout << "Bleached gaussian of radius " << radius << std::endl;

  cl_mem bufMask = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(cl_float)*dimxy[0]*dimxy[1], nullptr, nullptr);

  clerror = create_mask_gaussian(context, commandQueue, deviceId, bufMask, dimxy, radius, Ib, 1.f);

  if(clerror != CL_SUCCESS)
    return clerror;

  clerror = init_mask(context, commandQueue, deviceId, bufSeeds, bufPos, bufState, bufMask, Nworkers, particlesPerWorker, pi, dimxy);

  if(clerror != CL_SUCCESS)
    return clerror;

  OCLCheck(clReleaseMemObject(bufMask));

  return CL_SUCCESS;
}


cl_int init_homogeneous(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufSeeds, cl_mem bufPos, cl_mem bufState, unsigned int Nworkers, unsigned int particlesPerWorker, const float pi[2], const size_t dimxy[2])
{
  cl_int clerror;

  cl_kernel init_kernel;
  cl_program init_program;

  ParameterList initParameters;

  initParameters.push_back(Parameter("PARTICLESPERWORKER", (int)particlesPerWorker));
  initParameters.push_back(Parameter("DIMX", (int)dimxy[0]));
  initParameters.push_back(Parameter("DIMY", (int)dimxy[1]));
  initParameters.push_back(Parameter("PION", pi[0]));
  initParameters.push_back(Parameter("PIOFF", pi[1]));

  clerror = loadKernel("../common_cl/init_homogeneous.cl", "init_homogeneous", "-I ../common_cl/", context, deviceId, &init_program, &init_kernel, &initParameters);

  OCLCheck(clSetKernelArg(init_kernel, 0, sizeof(cl_mem), &bufSeeds));
  OCLCheck(clSetKernelArg(init_kernel, 1, sizeof(cl_mem), &bufPos));
  OCLCheck(clSetKernelArg(init_kernel, 2, sizeof(cl_mem), &bufState));

  size_t size = (size_t)Nworkers;

  clerror = clEnqueueNDRangeKernel(commandQueue, init_kernel, 1, nullptr, &size, nullptr, 0, nullptr, nullptr);

  if(clerror != CL_SUCCESS)
  {
    std::cerr << "Could not generate homogeneous initial conditions! OpenCL error: " << clerror << std::endl;
    return clerror;
  }

  OCLCheck(clFinish(commandQueue));

  OCLCheck(clReleaseKernel(init_kernel));
  OCLCheck(clReleaseProgram(init_program));

  return CL_SUCCESS;
}


cl_int bleach_mask(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufSeeds, cl_mem bufPos, cl_mem bufState, cl_mem bufMask, unsigned int Nworkers, unsigned int particlesPerWorker, const size_t dimxy[2])
{
  cl_int clerror;
  
  cl_kernel bleach_kernel;
  cl_program bleach_program;

  ParameterList bleachParameters;

  bleachParameters.push_back(Parameter("PARTICLESPERWORKER", (int)particlesPerWorker));
  bleachParameters.push_back(Parameter("DIMX", (int)dimxy[0]));
  bleachParameters.push_back(Parameter("DIMY", (int)dimxy[1]));
  
  clerror = loadKernel("../common_cl/stochastic_bleach.cl", "bleach_particles", "-I ../common_cl/", context, deviceId, &bleach_program, &bleach_kernel, &bleachParameters);

  if(clerror != CL_SUCCESS)
    return clerror;

  OCLCheck(clSetKernelArg(bleach_kernel, 0, sizeof(cl_mem), &bufSeeds));
  OCLCheck(clSetKernelArg(bleach_kernel, 1, sizeof(cl_mem), &bufPos));
  OCLCheck(clSetKernelArg(bleach_kernel, 2, sizeof(cl_mem), &bufState));
  OCLCheck(clSetKernelArg(bleach_kernel, 3, sizeof(cl_mem), &bufMask));

  size_t size = (size_t)Nworkers;

  clerror = clEnqueueNDRangeKernel(commandQueue, bleach_kernel, 1, nullptr, &size, nullptr, 0, nullptr, nullptr);

  if(clerror != CL_SUCCESS)
  {
    std::cerr << "Could not bleach! OpenCL error: " << clerror << std::endl;
    return clerror;
  }

  OCLCheck(clFinish(commandQueue));

  OCLCheck(clReleaseKernel(bleach_kernel));
  OCLCheck(clReleaseProgram(bleach_program));

  return CL_SUCCESS;
}


cl_int init_mask(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufSeeds, cl_mem bufPos, cl_mem bufState, cl_mem bufMask, unsigned int Nworkers, unsigned int particlesPerWorker, const float pi[2], const size_t dimxy[2])
{
  cl_int clerror;

  unsigned int Nparticles = Nworkers*particlesPerWorker;
  std::cout << "Computing initial conditions (masking) for " << Nparticles << " particles..." << std::flush;

  Timer initTimer;

  cl_program mask_program;
  cl_kernel mask_kernel;

  ParameterList parameters;
  parameters.push_back(Parameter("PARTICLESPERWORKER", (int)particlesPerWorker));
  parameters.push_back(Parameter("PION", pi[0]));
  parameters.push_back(Parameter("PIOFF", pi[1]));
  parameters.push_back(Parameter("DIMX", (int)dimxy[0]));
  parameters.push_back(Parameter("DIMY", (int)dimxy[1]));

  clerror = loadKernel("../common_cl/init_mask.cl", "init_mask", "-I ../common_cl/", context, deviceId, &mask_program, &mask_kernel, &parameters);

  if(clerror != CL_SUCCESS)
    return clerror;

  OCLCheck(clSetKernelArg(mask_kernel, 0, sizeof(cl_mem), &bufSeeds));
  OCLCheck(clSetKernelArg(mask_kernel, 1, sizeof(cl_mem), &bufPos));
  OCLCheck(clSetKernelArg(mask_kernel, 2, sizeof(cl_mem), &bufState));
  OCLCheck(clSetKernelArg(mask_kernel, 3, sizeof(cl_mem), &bufMask));

  size_t size = (size_t)Nworkers;

  clerror = clEnqueueNDRangeKernel(commandQueue, mask_kernel, 1, nullptr, &size, nullptr, 0, nullptr, nullptr);

  if(clerror != CL_SUCCESS)
  {
    std::cout << "OpenCL: Error during masking! Error code: " << clerror << std::endl;
    return -1;
  }
      
  OCLCheck(clFinish(commandQueue));
  
  std::cout << "Done. Elapsed time: " << initTimer.Elapsed() << "s" << std::endl;  
  
  return CL_SUCCESS;
}



