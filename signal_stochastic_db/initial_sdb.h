#ifndef SIGNAL_STOCHASTIC_DB_INITIAL_H
#define SIGNAL_STOCHASTIC_DB_INITIAL_H

#include<CL/opencl.h>

cl_int init_disk(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufSeeds, cl_mem bufPos, cl_mem bufState, unsigned int Nworkers, unsigned int particlesPerWorker, const float pi[2], float radius, float Ib, const size_t dimxy[2]);
cl_int init_gaussian(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufSeeds, cl_mem bufPos, cl_mem bufState, unsigned int Nworkers, unsigned int particlesPerWorker, const float pi[2], float radius, float Ib, const size_t dimxy[2]);
cl_int init_rect(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufSeeds, cl_mem bufPos, cl_mem bufState, unsigned int Nworkers, unsigned int particlesPerWorker, const float pi[2], const float dimRect[2], float Ib, const size_t dimxy[2]);
cl_int init_homogeneous(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufSeeds, cl_mem bufPos, cl_mem bufState, unsigned int Nworkers, unsigned int particlesPerWorker, const float pi[2], const size_t dimxy[2]);

cl_int bleach_mask(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufSeeds, cl_mem bufPos, cl_mem bufState, cl_mem bufMask, unsigned int Nworkers, unsigned int particlesPerWorker, const size_t dimxy[2]);
cl_int init_mask(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufSeeds, cl_mem bufPos, cl_mem bufState, cl_mem bufMask, unsigned int Nworkers, unsigned int particlesPerWorker, const float pi[2], const size_t dimxy[2]);

#endif // SIGNAL_STOCHASTIC_DB_INITIAL_H
