#include<iostream>
#include<libconfig.h++>

#include"opencl_common.h"
#include"signal_stochastic_db.h"
#include"timer.h"
#include"export_solution.h"


std::ostream& operator<<(std::ostream& stream, const simulation_ssdb_param_t& param)
{
  std::cout << "Simulation parameters:" << std::endl;
  std::cout << "Resolution N = " << param.N << ", padding = " << param.padding << std::endl;
  std::cout << "Effective resolution = [" << param.dimxy[0] << ", " << param.dimxy[1] << "]" << std::endl;
  std::cout << "Number of frames = " << param.frames << std::endl;
  std::cout << "Number of workers = " << param.workers << ", number of particles per worker = " << param.particlesPerWorker << ", total numbers of particles = " << param.Nparticles << std::endl;
  std::cout << "dT = " << param.dT << ", simulation steps = " << param.simSteps << std::endl;
  std::cout << "Diffusion-binding simulation: " << param.db << std::endl;
  if(param.db)
  {
    std::cout << "Kon = " << param.k[0] << ", koff = " << param.k[1] << std::endl;
    std::cout << "muon = " << param.mu[0] << ", muoff = " << param.mu[1] << std::endl;
    std::cout << "pion = " << param.pi[0] << ", pioff = " << param.pi[1] << std::endl;
  }
  std::cout << "Diffusion constant D = " << param.D << std::endl;
  std::cout << "Bleach coefficient = " << param.bleachCoef << std::endl;
  std::cout << "Bleach spot shape: " << param.bleachType << std::endl;
  if(param.bleachType == "disk" || param.bleachType == "gaussian")
    std::cout << "Radius = " << param.bleachRadius << std::endl;
  else if(param.bleachType == "rect")
    std::cout << "Rectangle dimension = [" << param.bleachRect[0] << ", " << param.bleachRect[1] << "]" << std::endl;
  std::cout << "Seed = " << param.seed;
}


bool loadSimulationParamFromConfigFile(simulation_ssdb_param_t& param, const char* configFilename)
{
  libconfig::Config cfg;

  try
  {
    cfg.readFile(configFilename);
  }
  catch(libconfig::ParseException& e)
  {
    std::cout << "Error when parsing simulation config file " << configFilename << " at line " << e.getLine() << std::endl;
    std::cout << e.getError() << std::endl;
    return false;
  }
  catch(libconfig::FileIOException& e)
  {
    std::cout << "Could not open simulation config file " << configFilename << std::endl;
    return false;
  }

  try
  {
    cfg.lookup("simulation.N");
    cfg.lookup("simulation.padding");
    cfg.lookup("simulation.frames");
    cfg.lookup("simulation.workers");
    cfg.lookup("simulation.particlesPerWorker");
    cfg.lookup("simulation.D");
    cfg.lookup("simulation.simSteps");
    cfg.lookup("simulation.dT");
    cfg.lookup("simulation.kon");
    cfg.lookup("simulation.koff");
    cfg.lookup("simulation.bleachType");
    cfg.lookup("simulation.bleachCoef");
    cfg.lookup("simulation.db");
    cfg.lookup("simulation.seeded");
    
    std::string bleachType;
    cfg.lookupValue("simulation.bleachType", bleachType);
    if(bleachType == "disk" || bleachType == "gaussian")
      cfg.lookup("simulation.bleachRadius");
    else if(bleachType == "rect")
    {
      const libconfig::Setting& bleachRect = cfg.lookup("simulation.bleachRect");
      if(bleachRect.getLength() != 2)
      {
	std::cout << "Badly formed array for bleachRect!" << std::endl;
	return false;
      }
    }
    else if(bleachType == "none");
    else
    {
      std::cerr << "Could not recognise bleach type: " << bleachType << std::endl;
      return false;
    }
  }
  catch(libconfig::SettingNotFoundException& e)
  {
    std::cout << "Error when loading simulation parameters from config file! Could not find: " << e.getPath() << std::endl;
    return false;
  }
  
  cfg.lookupValue("simulation.N", param.N);
  cfg.lookupValue("simulation.padding", param.padding);
  param.dimxy[0] = param.N+2*param.padding;
  param.dimxy[1] = param.N+2*param.padding;
  cfg.lookupValue("simulation.frames", param.frames);
  cfg.lookupValue("simulation.workers", param.workers);
  cfg.lookupValue("simulation.particlesPerWorker", param.particlesPerWorker);
  param.Nparticles = param.workers*param.particlesPerWorker;
  cfg.lookupValue("simulation.bleachCoef", param.bleachCoef);
  cfg.lookupValue("simulation.simSteps", param.simSteps);
  cfg.lookupValue("simulation.dT", param.dT);
  cfg.lookupValue("simulation.kon", param.k[0]);
  cfg.lookupValue("simulation.koff", param.k[1]);
  param.mu[0] = 1.f/param.k[0];
  param.mu[1] = 1.f/param.k[1];
  param.pi[0] = param.k[1]/(param.k[0]+param.k[1]);
  param.pi[1] = param.k[0]/(param.k[0]+param.k[1]);
  cfg.lookupValue("simulation.D", param.D);
  cfg.lookupValue("simulation.db", param.db);
  cfg.lookupValue("simulation.seeded", param.seeded);

  if(param.seeded)
  {
    if(!cfg.lookupValue("simulation.seed", param.seed))
    {
      std::cout << "Warning! Simulation seed should be provided! Generated instead." << std::endl;
      param.seed = std::time(nullptr);
    }
  }
  else
    param.seed = time(nullptr);

  cfg.lookupValue("simulation.bleachType", param.bleachType);
  if(param.bleachType == "disk" || param.bleachType == "gaussian")
  {
    cfg.lookupValue("simulation.bleachRadius", param.bleachRadius);
  }
  else if(param.bleachType == "rect")
  {
    const libconfig::Setting& bleachRect = cfg.lookup("simulation.bleachRect");
    param.bleachRect[0] = bleachRect[0];
    param.bleachRect[1] = bleachRect[1];
  }

  return true;
}


cl_int signal_stochastic_db(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufSeeds, cl_mem bufPos, cl_mem bufState, cl_uint Nworkers, cl_uint particlesPerWorker, cl_uint simSteps, float D, float dT, const float mu[2], bool isDB, const size_t dimxy[2], unsigned int n, std::vector<Eigen::ArrayXXi>& Ct)
{
  unsigned int Nparticles = Nworkers*particlesPerWorker;
  cl_int clerror;
  
  std::cout << "Initialising kernels..." << std::flush;

  // We now initialise the kernel we will run on the device
  cl_program dbs_program;
  cl_program bin2d_program;
  cl_kernel dbs_kernel;
  cl_kernel bin2d_kernel;

  cl_mem bufBin = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_int)*dimxy[0]*dimxy[1]*n, nullptr, nullptr);

  // We fill the output buffer with 0 data as some OpenCL implementation does not do it for us (POCL for example)
  cl_int* pBinInit = new cl_int[dimxy[0]*dimxy[1]*n];
  memset(pBinInit, 0x00, sizeof(cl_int)*dimxy[0]*dimxy[1]*n);
  OCLCheck(clEnqueueWriteBuffer(commandQueue, bufBin, CL_TRUE, 0, sizeof(cl_int)*dimxy[0]*dimxy[1]*n, pBinInit, 0, nullptr, nullptr));
  delete[] pBinInit;

  ParameterList sdbParameters;
  sdbParameters.push_back(Parameter("PARTICLESPERWORKER", (int)particlesPerWorker));
  sdbParameters.push_back(Parameter("SIMSTEPS", (int)simSteps));
  sdbParameters.push_back(Parameter("D", D));
  sdbParameters.push_back(Parameter("DTSIM", dT/simSteps));
  sdbParameters.push_back(Parameter("MUON", mu[0]));
  sdbParameters.push_back(Parameter("MUOFF", mu[1]));
  sdbParameters.push_back(Parameter("DIMX", (int)dimxy[0]));
  sdbParameters.push_back(Parameter("DIMY", (int)dimxy[1]));
  if(isDB)
    sdbParameters.push_back(Parameter("DB", (int)isDB));

  ParameterList binParameters;
  binParameters.push_back(Parameter("PARTICLESPERWORKER", (int)particlesPerWorker));
  binParameters.push_back(Parameter("DIMX", (int)dimxy[0]));
  binParameters.push_back(Parameter("DIMY", (int)dimxy[1]));

  loadKernel("../signal_stochastic_db/stochastic_db.cl", "stochastic_db", "-I ../common_cl/", context, deviceId, &dbs_program, &dbs_kernel, &sdbParameters);
  loadKernel("../common_cl/bin2d.cl", "bin2d", "-I ../common_cl/", context, deviceId, &bin2d_program, &bin2d_kernel, &binParameters);
  
  OCLCheck(clSetKernelArg(dbs_kernel, 0, sizeof(cl_mem), &bufSeeds));
  OCLCheck(clSetKernelArg(dbs_kernel, 1, sizeof(cl_mem), &bufPos));
  OCLCheck(clSetKernelArg(dbs_kernel, 2, sizeof(cl_mem), &bufState));

  OCLCheck(clSetKernelArg(bin2d_kernel, 0, sizeof(cl_mem), &bufPos));
  OCLCheck(clSetKernelArg(bin2d_kernel, 1, sizeof(cl_mem), &bufState));
  OCLCheck(clSetKernelArg(bin2d_kernel, 2, sizeof(cl_mem), &bufBin));

  std::cout << "Done" << std::endl;

  size_t dim_dbs = Nworkers;

  std::cout << "Starting simulation with " << Nparticles << " particles, on " << Nworkers << " workers." << std::endl;

  Timer totalPerfTimer;

  for(unsigned int i = 0; i < n; i++)
  {
    std::cout << "Pass " << i << "... " << std::endl;
    std::cout << "Simulating... " << std::flush;
    
    Timer myPerfTimer;
    
    clerror = clEnqueueNDRangeKernel(commandQueue, dbs_kernel, 1, nullptr, &dim_dbs, nullptr, 0, nullptr, nullptr);

    if(clerror != CL_SUCCESS)
    {
      std::cout << "OpenCL: Error during simulation pass " << i << "! Error code: " << clerror << std::endl;
      return clerror;
    }

    OCLCheck(clFinish(commandQueue));

    std::cout << "Done. Execution time: " << myPerfTimer.Elapsed() << "s"  << std::endl;

    std::cout << "Binning... " << std::flush;

    myPerfTimer = Timer();
    
    OCLCheck(clSetKernelArg(bin2d_kernel, 3, sizeof(cl_uint), &i));
    clerror = clEnqueueNDRangeKernel(commandQueue, bin2d_kernel, 1, nullptr, &dim_dbs, nullptr, 0, nullptr, nullptr);

    if(clerror != CL_SUCCESS)
    {
      std::cout << "OpenCL: Error during binning pass " << i << "! Error code: " << clerror << std::endl;
      return clerror;
    }
      
    OCLCheck(clFinish(commandQueue));
    
    std::cout << "Done. Execution time: " << myPerfTimer.Elapsed() << "s" << std::endl;
  }
  
  std::cout << "Simulation done. Elapsed time: " << totalPerfTimer.Elapsed() << "s" << std::endl;

  std::cout << "Retrieving results..." << std::flush;

  cl_int* pCt = new cl_int[dimxy[0]*dimxy[1]*n];
  OCLCheck(clEnqueueReadBuffer(commandQueue, bufBin, CL_TRUE, 0, sizeof(cl_int)*dimxy[0]*dimxy[1]*n, pCt, 0, nullptr, nullptr));

  for(int i = 0; i < n; i++)
    Ct.push_back(Eigen::Map<Eigen::ArrayXXi>(&pCt[dimxy[0]*dimxy[1]*i], dimxy[0], dimxy[1]));

  std::cout << "Done" << std::endl;
  
  std::cout << "Cleaning..." << std::flush;
  OCLCheck(clReleaseMemObject(bufBin));
  OCLCheck(clReleaseKernel(bin2d_kernel));
  OCLCheck(clReleaseKernel(dbs_kernel));
  OCLCheck(clReleaseProgram(bin2d_program));
  OCLCheck(clReleaseProgram(dbs_program));
  std::cout << "Done" << std::endl;

  return CL_SUCCESS;
}


cl_int signal_stochastic_db_one_step(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufSeeds, cl_mem bufPos, cl_mem bufState, cl_uint Nworkers, cl_uint particlesPerWorker, cl_uint simSteps, float D, float dT, const float mu[2], bool isDB, const size_t dimxy[2])
{
  unsigned int Nparticles = Nworkers*particlesPerWorker;
  cl_int clerror;
  
  // We now initialise the kernel we will run on the device
  cl_program dbs_program;
  cl_kernel dbs_kernel;

  ParameterList sdbParameters;
  sdbParameters.push_back(Parameter("PARTICLESPERWORKER", (int)particlesPerWorker));
  sdbParameters.push_back(Parameter("D", D));
  sdbParameters.push_back(Parameter("SIMSTEPS", (int)simSteps));
  sdbParameters.push_back(Parameter("DTSIM", dT/simSteps));
  sdbParameters.push_back(Parameter("MUON", mu[0]));
  sdbParameters.push_back(Parameter("MUOFF", mu[1]));
  sdbParameters.push_back(Parameter("DIMX", (int)dimxy[0]));
  sdbParameters.push_back(Parameter("DIMY", (int)dimxy[1]));
  if(isDB)
    sdbParameters.push_back(Parameter("DB", (int)isDB));

  loadKernel("../signal_stochastic_db/stochastic_db.cl", "stochastic_db", "-I ../common_cl/", context, deviceId, &dbs_program, &dbs_kernel, &sdbParameters);
  
  OCLCheck(clSetKernelArg(dbs_kernel, 0, sizeof(cl_mem), &bufSeeds));
  OCLCheck(clSetKernelArg(dbs_kernel, 1, sizeof(cl_mem), &bufPos));
  OCLCheck(clSetKernelArg(dbs_kernel, 2, sizeof(cl_mem), &bufState));

  size_t dim_dbs = Nworkers;
  
  clerror = clEnqueueNDRangeKernel(commandQueue, dbs_kernel, 1, nullptr, &dim_dbs, nullptr, 0, nullptr, nullptr);

  if(clerror != CL_SUCCESS)
  {
    std::cout << "OpenCL: Error during simulation! Error code: " << clerror << std::endl;
    return clerror;
  }

  OCLCheck(clFinish(commandQueue));
  
  OCLCheck(clReleaseKernel(dbs_kernel));
  OCLCheck(clReleaseProgram(dbs_program));

  return CL_SUCCESS;
}


cl_int get_frame_from_particles(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufPos, cl_mem bufState, cl_uint Nworkers, cl_uint particlesPerWorker, const size_t dimxy[2], Eigen::ArrayXXi& outFrame)
{
  cl_int clerror;
  cl_program bin2d_program;
  cl_kernel bin2d_kernel;

  cl_mem bufBin = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_int)*dimxy[0]*dimxy[1], nullptr, nullptr);

  // We fill the output buffer with 0 data as some OpenCL implementation does not do it for us (POCL for example)
  cl_int* pBinInit = new cl_int[dimxy[0]*dimxy[1]];
  memset(pBinInit, 0x00, sizeof(cl_int)*dimxy[0]*dimxy[1]);
  OCLCheck(clEnqueueWriteBuffer(commandQueue, bufBin, CL_TRUE, 0, sizeof(cl_int)*dimxy[0]*dimxy[1], pBinInit, 0, nullptr, nullptr));
  delete[] pBinInit;

  ParameterList binParameters;
  binParameters.push_back(Parameter("PARTICLESPERWORKER", (int)particlesPerWorker));
  binParameters.push_back(Parameter("DIMX", (int)dimxy[0]));
  binParameters.push_back(Parameter("DIMY", (int)dimxy[1]));

  clerror = loadKernel("../common_cl/bin2d.cl", "bin2d", "-I ../common_cl/", context, deviceId, &bin2d_program, &bin2d_kernel, &binParameters);

  if(clerror != CL_SUCCESS)
    return clerror;

  cl_uint passZero = 0;
  OCLCheck(clSetKernelArg(bin2d_kernel, 0, sizeof(cl_mem), &bufPos));
  OCLCheck(clSetKernelArg(bin2d_kernel, 1, sizeof(cl_mem), &bufState));
  OCLCheck(clSetKernelArg(bin2d_kernel, 2, sizeof(cl_mem), &bufBin));
  OCLCheck(clSetKernelArg(bin2d_kernel, 3, sizeof(cl_uint), &passZero));

  size_t dim_dbs = Nworkers;
  clerror = clEnqueueNDRangeKernel(commandQueue, bin2d_kernel, 1, nullptr, &dim_dbs, nullptr, 0, nullptr, nullptr);

  if(clerror != CL_SUCCESS)
  {
    std::cout << "OpenCL: Error when binning! Error code: " << clerror << std::endl;
    return clerror;
  }

  OCLCheck(clFinish(commandQueue));

  cl_int* pFrame = new cl_int[dimxy[0]*dimxy[1]];
  OCLCheck(clEnqueueReadBuffer(commandQueue, bufBin, CL_TRUE, 0, sizeof(cl_int)*dimxy[0]*dimxy[1], pFrame, 0, NULL, NULL ));

  outFrame = Eigen::Map<Eigen::ArrayXXi>(pFrame, dimxy[0], dimxy[1]);

  delete[] pFrame;

  OCLCheck(clReleaseMemObject(bufBin));
  OCLCheck(clReleaseKernel(bin2d_kernel));
  OCLCheck(clReleaseProgram(bin2d_program));

  return CL_SUCCESS;
}


#define STATE_BOUNDED (1 << 0)
#define STATE_INACTIVE (1 << 1)


Eigen::Array22i dump_particles_state(cl_command_queue commandQueue, cl_mem bufState, unsigned int particlesCount)
{
  Eigen::Array22i out = Eigen::Array22i::Zero();

  cl_uchar* pState = new cl_uchar[particlesCount];

  OCLCheck(clEnqueueReadBuffer(commandQueue, bufState, CL_TRUE, 0, sizeof(cl_uchar)*particlesCount, pState, 0, nullptr, nullptr));

  for(unsigned int i = 0; i < particlesCount; i++)
  {
    bool bounded = (bool)(pState[i] & ~STATE_BOUNDED);
    bool inactive = (bool)(pState[i] & ~STATE_INACTIVE);

    if(bounded && inactive)
      out(0, 0) += 1;
    else if(bounded && !inactive)
      out(1, 0) += 1;
    else if(!bounded && inactive)
      out(0, 1) += 1;
    else
      out(1, 1) += 1;
  }

  delete[] pState;

  return out;
}
