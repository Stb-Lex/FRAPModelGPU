#ifndef SIGNAL_STOCHASTIC_H
#define SIGNAL_STOCHASTIC_H

#include<vector>
#include<Eigen/Dense>
#include<iostream>

struct simulation_ssdb_param_t
{
  unsigned int N;
  unsigned int padding;
  size_t dimxy[2];
  unsigned int frames;
  
  unsigned int workers;
  unsigned int particlesPerWorker;
  unsigned int Nparticles;
  
  float bleachCoef;
  
  unsigned int simSteps;
  float dT;

  bool db;
  float k[2];
  float mu[2];
  float pi[2];
  float D;

  std::string bleachType;
  float bleachRadius;
  float bleachRect[2];

  bool seeded;
  unsigned int seed;
};

std::ostream& operator<<(std::ostream& stream, const simulation_ssdb_param_t& param);

bool loadSimulationParamFromConfigFile(simulation_ssdb_param_t& param, const char* szConfigFilename);

cl_int signal_stochastic_db(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufSeeds, cl_mem bufPos, cl_mem bufState, cl_uint Nworkers, cl_uint particlesPerWorker, cl_uint simSteps, float D, float dT, const float mu[2], bool isDB, const size_t dimxy[2], unsigned int n, std::vector<Eigen::ArrayXXi>& Ct);

cl_int signal_stochastic_db_one_step(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufSeeds, cl_mem bufPos, cl_mem bufState, cl_uint Nworkers, cl_uint particlesPerWorker, cl_uint simSteps, float D, float dT, const float mu[2], bool isDB, const size_t dimxy[2]);

cl_int get_frame_from_particles(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufPos, cl_mem bufState, cl_uint Nworkers, cl_uint particlesPerWorker, const size_t dimxy[2], Eigen::ArrayXXi& outFrame);

Eigen::Array22i dump_particles_state(cl_command_queue commandQueue, cl_mem bufState, unsigned int particlesCount);

#endif // SIGNAL_STOCHASTIC_H
