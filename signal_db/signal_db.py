import sys
import numpy as np
import matplotlib.pyplot as plt
import scipy.misc
import scipy.ndimage

sys.path.append("../validate/")
import common

N = 256
M = 128
upscale = 1
dimxy = [N+2*M, N+2*M]
frames = 500

k = [0.0000001, 1.]
pi = [k[1]/(k[0]+k[1]), k[0]/(k[0]+k[1])]
D = 60.
dT = 1

R = 64
alpha = 0.6

# We first generate the bleach mask
Y, X = np.ogrid[-upscale*(dimxy[0]/2):upscale*(dimxy[0]/2), -upscale*(dimxy[1]/2):upscale*(dimxy[1]/2)]
X = X + 0.5
Y = Y + 0.5
diskMask = X*X+Y*Y < upscale*upscale*R*R
bleachMask = np.ones([upscale*dimxy[0], upscale*dimxy[1]])
bleachMask[diskMask] = alpha

# Then we downscale
#bleachMask = scipy.ndimage.zoom(bleachMask, 1/upscale, order=2, prefilter=True)
#bleachMask = scipy.misc.imresize(bleachMask, dimxy, mode='F')

C0 = bleachMask
F_C0 = np.fft.fft2(C0)
F_U0 = pi[0]*F_C0
F_B0 = pi[1]*F_C0

F_Ut = np.zeros([dimxy[0], dimxy[1], frames], dtype=np.complex64)
F_Bt = np.zeros([dimxy[0], dimxy[1], frames], dtype=np.complex64)

Y, X = np.ogrid[-(dimxy[0]/2):(dimxy[0]/2), -(dimxy[1]/2):(dimxy[1]/2)]
X = X*2*np.pi/dimxy[0]
Y = Y*2*np.pi/dimxy[1]

XSISQ = np.fft.ifftshift(X*X+Y*Y)

QQ11 = -(k[0] - k[1] + (D**2*XSISQ**2 + 2*D*k[0]*XSISQ - 2*D*k[1]*XSISQ + k[0]**2 + 2*k[0]*k[1] + k[1]**2)**(1/2) + D*XSISQ)/(2*k[0])
QQ12 = -(k[0] - k[1] - (D**2*XSISQ**2 + 2*D*k[0]*XSISQ - 2*D*k[1]*XSISQ + k[0]**2 + 2*k[0]*k[1] + k[1]**2)**(1/2) + D*XSISQ)/(2*k[0])

LL11 = -((k[0] + k[1] + (D**2*XSISQ**2 + 2*D*k[0]*XSISQ - 2*D*k[1]*XSISQ + k[0]**2 + 2*k[0]*k[1] + k[1]**2)**(1/2) + D*XSISQ))/2
LL22 = -((k[0] + k[1] - (D**2*XSISQ**2 + 2*D*k[0]*XSISQ - 2*D*k[1]*XSISQ + k[0]**2 + 2*k[0]*k[1] + k[1]**2)**(1/2) + D*XSISQ))/2

QQinv11 = -k[0]/(2*k[0]*k[1] + k[0]**2 + k[1]**2 + D**2*XSISQ**2 + 2*D*k[0]*XSISQ - 2*D*k[1]*XSISQ)**(1/2)
QQinv12 = -(k[0] - k[1] - (D**2*XSISQ**2 + 2*D*k[0]*XSISQ - 2*D*k[1]*XSISQ + k[0]**2 + 2*k[0]*k[1] + k[1]**2)**(1/2) + D*XSISQ)/(2*(2*k[0]*k[1] + k[0]**2 + k[1]**2 + D**2*XSISQ**2 + 2*D*k[0]*XSISQ - 2*D*k[1]*XSISQ)**(1/2))
QQinv21 = -QQinv11
QQinv22 = (k[0] - k[1] + (D**2*XSISQ**2 + 2*D*k[0]*XSISQ - 2*D*k[1]*XSISQ + k[0]**2 + 2*k[0]*k[1] + k[1]**2)**(1/2) + D*XSISQ)/(2*(2*k[0]*k[1] + k[0]**2 + k[1]**2 + D**2*XSISQ**2 + 2*D*k[0]*XSISQ - 2*D*k[1]*XSISQ)**(1/2))

CONST11 = QQ11 * (QQinv11 * F_U0 + QQinv12 * F_B0)
CONST12 = QQ12 * (QQinv21 * F_U0 + QQinv22 * F_B0)
CONST21 = QQinv11 * F_U0 + QQinv12 * F_B0
CONST22 = QQinv21 * F_U0 + QQinv22 * F_B0

for t in range(0, frames):
    T = (t+1) * dT
    CONST1 = np.exp(LL11 * T)
    CONST2 = np.exp(LL22 * T)
    F_Ut[:, :, t] = CONST11 * CONST1 + CONST12 * CONST2
    F_Bt[:, :, t] = CONST21 * CONST1 + CONST22 * CONST2

data = np.zeros([dimxy[0], dimxy[1], frames])
for t in range(0, frames):
    data[:, :, t] = np.abs(np.fft.ifft2(F_Ut[:, :, t] + F_Bt[:, :, t]));

common.export_pickle(sys.argv[1], data, dimxy, frames)
