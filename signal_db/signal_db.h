#ifndef SIGNAL_DB_H
#define SIGNAL_DB_H

#include<CL/opencl.h>
#include<vector>
#include<Eigen/Dense>

cl_int signal_db(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufC0r, bool db, const float k[2], float D, float dT, const size_t dimxy[2], unsigned int n, std::vector<Eigen::ArrayXXf>& Ct);

cl_int signal_db_fft(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufU[2], cl_mem bufB[2], bool db, const float k[2], float D, float dT, const size_t dimxy[2], unsigned int n, std::vector<Eigen::ArrayXXf>& Ct);

// Used by the bf version of signal_db (diffusion between bleaching)
cl_int signal_db_one_frame(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufU[2], cl_mem bufB[2], cl_mem bufC, bool db, const float k[2], float D, float dT, const size_t dimxy[2]);

Eigen::ArrayXXf get_frame(cl_command_queue commandQueue, cl_mem buf, const size_t dimxy[2]);
Eigen::ArrayXXf get_frame_fft(cl_context context, cl_command_queue commandQueue, cl_mem buf[2], const size_t dimxy[2]);

#endif // SIGNAL_DB_H
