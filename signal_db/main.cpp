#include<CL/opencl.h>
#include<iostream>
#include<Eigen/Dense>
#include<clFFT.h>

#include"signal_db.h"
#include"mask.h"
#include"export_solution.h"
#include"timer.h"
#include"opencl_common.h"

int main(int argc, char* argv[])
{
  if(argc == 1)
  {
    std::cout << "Usage (disk): signal_db outFilename steps disk radius" << std::endl;
    std::cout << "Usage (rectangle): signal_db outFilename steps rect dimx dimy" << std::endl;
    std::cout << "Usage (gaussian): signal_db outFilename steps gaussian radius" << std::endl;
    return 0;
  }

  std::string szOutFilename = argv[1];

  char bleachType;

  if(strcmp(argv[3], "disk") == 0)
    bleachType = 'd';
  else if(strcmp(argv[3], "rect") == 0)
    bleachType = 'r';
  else if(strcmp(argv[3], "gaussian") == 0)
    bleachType = 'g';
  else
  {
    std::cerr << "Bleaching type unrecognised... Bye bye" << std::endl;
    return -1;
  }
  
  // Problem related constants
  const float Iu = 1.0f;
  const float Ib = 0.24f;
  const float k[2] = {1.f, 1.f}; // kon, koff
  const float D = 800.f;

  const unsigned int N = 256;
  const unsigned int Pad = 128;
  const unsigned int n = atoi(argv[2]); // Number of slices/time steps
  const float dT = 0.265f; // s

  const bool db = true;

  const size_t dimxy[2] = {2*Pad+N, 2*Pad+N};

  // OpenCL related vars  
  cl_platform_id platformId;
  cl_device_id deviceId;
  cl_context context;
  cl_command_queue commandQueue;
  cl_int clerror;
  unsigned int deviceCount;
  unsigned int platformCount;

  // We initialise OpenCL and clFFT
  Timer myPerfTimer;
  std::cout << "Initialising OpenCL and clFFT... " << std::flush;

  OCLCheck(initOpenCL(platformId, context, deviceId, commandQueue, CL_DEVICE_TYPE_GPU));
  //clerror = initOpenCLByPlatformName(platformId, context, deviceId, commandQueue, "Portable Computing Language", CL_DEVICE_TYPE_CPU);

  if(clerror != CL_SUCCESS)
    return -1;

  clfftSetupData fftSetup;
  clerror = clfftInitSetupData(&fftSetup);
  clerror = clfftSetup(&fftSetup);

  if(clerror != CL_SUCCESS)
  {
    std::cerr << "clFFT: Could not setup! Error code: " << clerror << std::endl;
    return -1;
  }

  std::cout << "Done. Elapsed time: " << myPerfTimer.Elapsed() << "s" << std::endl;

  std::cout << "Generating initial conditions..." << std::flush;
  myPerfTimer = Timer();

  cl_mem bufC0 = nullptr;
  
  std::vector<Eigen::ArrayXXf> Ct;
  
  switch(bleachType)
  {
  case 'd':{
    float bleachRadius = (float)atoi(argv[4]);
    clerror = create_mask_disk(context, commandQueue, deviceId, bufC0, dimxy, bleachRadius, Ib, Iu, 2);
    break;
  }
  case 'r':{
    float bleachRect[2];
    bleachRect[0] = (float)atoi(argv[4]);
    bleachRect[1] = (float)atoi(argv[5]);
    clerror = create_mask_rect(context, commandQueue, deviceId, bufC0, dimxy, bleachRect, Ib, Iu, 2);
    break;
  }
  case 'g':{
    float bleachRadius = (float)atoi(argv[4]);
    clerror = create_mask_gaussian(context, commandQueue, deviceId, bufC0, dimxy, bleachRadius, Ib, Iu);
    break;
  }
  }
    
  if(clerror != CL_SUCCESS)
    return -1;

  std::cout << "Done. Elapsed time: " << myPerfTimer.Elapsed() << "s" << std::endl;

  std::cout << "Simulating..." << std::flush;
  
  myPerfTimer = Timer();

  if(signal_db(context, commandQueue, deviceId, bufC0, db, k, D, dT, dimxy, n, Ct) != CL_SUCCESS)
    return -1;

  
  std::cout << "Done. Elapsed time: " << myPerfTimer.Elapsed() << "s" << std::endl;

  std::cout << "Saving on disk under " << szOutFilename << "..." << std::flush;
  export_csv<Eigen::ArrayXXf>(szOutFilename, Ct);
  std::cout << "Done" << std::endl;
  
  clfftTeardown();
  OCLCheck(clReleaseCommandQueue(commandQueue));
  OCLCheck(clReleaseContext(context));
  
  return 0;
}

