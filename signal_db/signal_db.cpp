#include<clFFT.h>
#include<iostream>
#include<sstream>
#include<fstream>
#include<string>
#include<cstring>
#include<cstdlib>

#include"opencl_common.h"
#include"signal_db.h"

cl_int signal_db(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufC0r, bool db, const float k[2], float D, float dT, const size_t dimxy[2], unsigned int n, std::vector<Eigen::ArrayXXf>& Ct)
{
  cl_int clerror;

  // We load initial conditions on the device
  cl_mem bufC0i = createEmptyBuffer(context, CL_MEM_READ_WRITE, sizeof(cl_float)*dimxy[0]*dimxy[1]);

  cl_mem bufC0[2];
  bufC0[0] = bufC0r;
  bufC0[1] = bufC0i;
  
  // We now transform the initial condition into fft space on the GPU
  // The good thing here is that the result will be already stored on the device so no more memory transfers are needed
  
  clfftPlanHandle planHandle;

  OCLCheck(clfftCreateDefaultPlan(&planHandle, context, CLFFT_2D, dimxy));

  OCLCheck(clfftSetPlanPrecision(planHandle, CLFFT_SINGLE));
  OCLCheck(clfftSetLayout(planHandle, CLFFT_COMPLEX_PLANAR, CLFFT_COMPLEX_PLANAR));
  OCLCheck(clfftSetResultLocation(planHandle, CLFFT_INPLACE));

  OCLCheck(clfftBakePlan(planHandle, 1, &commandQueue, nullptr, nullptr));

  OCLCheck(clfftEnqueueTransform(planHandle, CLFFT_FORWARD, 1, &commandQueue, 0, nullptr, nullptr, bufC0, nullptr, nullptr));

  OCLCheck(clFinish(commandQueue));

  OCLCheck(clfftDestroyPlan(&planHandle));

  // We now do the simulation in fft space on the GPU
  // We first allocate the space for the solutions on the GPU
  cl_mem bufCt[2];
  bufCt[0] = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_float)*dimxy[0]*dimxy[1]*n, nullptr, nullptr);
  bufCt[1] = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_float)*dimxy[0]*dimxy[1]*n, nullptr, nullptr);

  // We now initialise the kernel we will run on the device
  cl_program db_program;
  cl_kernel db_kernel;

  ParameterList dbParameters;
  if(db)
    dbParameters.push_back(Parameter("DB", 1));

  dbParameters.push_back(Parameter("KON", k[0]));
  dbParameters.push_back(Parameter("KOFF", k[1]));
  dbParameters.push_back(Parameter("D", D));
  dbParameters.push_back(Parameter("FRAMECOUNT", (int)n));
  dbParameters.push_back(Parameter("DT", dT));

  clerror = loadKernel("../common_cl/db.cl", "db", "-I ../common_cl/", context, deviceId, &db_program, &db_kernel, &dbParameters);

  if(clerror != CL_SUCCESS)
    return clerror;

  cl_uint2 cl_dimxy = {(cl_uint)dimxy[0], (cl_uint)dimxy[1]};

  // Initial conditions in fft space
  OCLCheck(clSetKernelArg(db_kernel, 0, sizeof(cl_mem), &bufC0[0]));
  OCLCheck(clSetKernelArg(db_kernel, 1, sizeof(cl_mem), &bufC0[1]));

  // Simulation at timestep t in fft space
  OCLCheck(clSetKernelArg(db_kernel, 2, sizeof(cl_mem), &bufCt[0]));
  OCLCheck(clSetKernelArg(db_kernel, 3, sizeof(cl_mem), &bufCt[1]));

  size_t problemDim[2] = {dimxy[0], dimxy[1]};

  clerror = clEnqueueNDRangeKernel(commandQueue, db_kernel, 2, nullptr, problemDim, nullptr, 0, nullptr, nullptr);

  if(clerror != CL_SUCCESS)
  {
    std::cout << "OpenCL: Could not enqueue the db kernel! Error code:" << clerror << std::endl;
    return clerror;
  }

  OCLCheck(clFinish(commandQueue));

  // We now do inverse transform of the data

  clfftPlanHandle backPlanHandle;

  OCLCheck(clfftCreateDefaultPlan(&backPlanHandle, context, CLFFT_2D, dimxy));

  OCLCheck(clfftSetPlanPrecision(backPlanHandle, CLFFT_SINGLE));
  OCLCheck(clfftSetLayout(backPlanHandle, CLFFT_COMPLEX_PLANAR, CLFFT_COMPLEX_PLANAR));
  OCLCheck(clfftSetResultLocation(backPlanHandle, CLFFT_INPLACE));

  OCLCheck(clfftSetPlanBatchSize(backPlanHandle, n));

  OCLCheck(clfftBakePlan(backPlanHandle, 1, &commandQueue, nullptr, nullptr));

  OCLCheck(clfftEnqueueTransform(backPlanHandle, CLFFT_BACKWARD, 1, &commandQueue, 0, nullptr, nullptr, bufCt, nullptr, nullptr));

  OCLCheck(clFinish(commandQueue));

  OCLCheck(clfftDestroyPlan(&backPlanHandle));

  cl_float* pCt = new cl_float[dimxy[0]*dimxy[1]*n];
  clerror = clEnqueueReadBuffer(commandQueue, bufCt[0], CL_TRUE, 0, sizeof(cl_float)*dimxy[0]*dimxy[1]*n, pCt, 0, NULL, NULL );

  for(int i = 0; i < n; i++)
  {
    Eigen::ArrayXXf curSlice = Eigen::Map<Eigen::ArrayXXf>(&pCt[dimxy[0]*dimxy[1]*i], dimxy[0], dimxy[1]);
    Ct.push_back(curSlice);
  }

  delete[] pCt;

  OCLCheck(clReleaseMemObject(bufCt[0]));
  OCLCheck(clReleaseMemObject(bufCt[1]));
  OCLCheck(clReleaseMemObject(bufC0i));
  OCLCheck(clReleaseProgram(db_program));
  OCLCheck(clReleaseKernel(db_kernel));

  return CL_SUCCESS;
}


cl_int signal_db_fft(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufU[2], cl_mem bufB[2], bool db, const float k[2], float D, float dT, const size_t dimxy[2], unsigned int n, std::vector<Eigen::ArrayXXf>& Ct)
{
  cl_int clerror;

  cl_mem bufCt[2];
  bufCt[0] = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_float)*dimxy[0]*dimxy[1]*n, nullptr, nullptr);
  bufCt[1] = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_float)*dimxy[0]*dimxy[1]*n, nullptr, nullptr);
  
  // We now transform U and B condition into fft space on the GPU
  clfftPlanHandle forwardPlanHandle;
  OCLCheck(clfftCreateDefaultPlan(&forwardPlanHandle, context, CLFFT_2D, dimxy));

  OCLCheck(clfftSetPlanPrecision(forwardPlanHandle, CLFFT_SINGLE));
  OCLCheck(clfftSetLayout(forwardPlanHandle, CLFFT_COMPLEX_PLANAR, CLFFT_COMPLEX_PLANAR));
  OCLCheck(clfftSetResultLocation(forwardPlanHandle, CLFFT_INPLACE));

  OCLCheck(clfftBakePlan(forwardPlanHandle, 1, &commandQueue, nullptr, nullptr));

  OCLCheck(clfftEnqueueTransform(forwardPlanHandle, CLFFT_FORWARD, 1, &commandQueue, 0, nullptr, nullptr, bufU, nullptr, nullptr));
  OCLCheck(clfftEnqueueTransform(forwardPlanHandle, CLFFT_FORWARD, 1, &commandQueue, 0, nullptr, nullptr, bufB, nullptr, nullptr));

  OCLCheck(clFinish(commandQueue));

  clfftDestroyPlan(&forwardPlanHandle);

  // We now initialise the kernel we will run on the device  
  cl_program db_program;
  cl_kernel db_kernel;

  ParameterList dbParameters;
  if(db)
    dbParameters.push_back(Parameter("DB", 1));
  
  dbParameters.push_back(Parameter("KON", k[0]));
  dbParameters.push_back(Parameter("KOFF", k[1]));
  dbParameters.push_back(Parameter("D", D));
  dbParameters.push_back(Parameter("FRAMECOUNT", (int)n));
  dbParameters.push_back(Parameter("DT", dT));

  clerror = loadKernel("../common_cl/db_fft.cl", "db_fft", "-I ../common_cl/", context, deviceId, &db_program, &db_kernel, &dbParameters);

  if(clerror != CL_SUCCESS)
    return clerror;

  OCLCheck(clSetKernelArg(db_kernel, 0, sizeof(cl_mem), &bufU[0]));
  OCLCheck(clSetKernelArg(db_kernel, 1, sizeof(cl_mem), &bufU[1]));
  OCLCheck(clSetKernelArg(db_kernel, 2, sizeof(cl_mem), &bufB[0]));
  OCLCheck(clSetKernelArg(db_kernel, 3, sizeof(cl_mem), &bufB[1]));
  OCLCheck(clSetKernelArg(db_kernel, 4, sizeof(cl_mem), &bufCt[0]));
  OCLCheck(clSetKernelArg(db_kernel, 5, sizeof(cl_mem), &bufCt[1]));

  clerror = clEnqueueNDRangeKernel(commandQueue, db_kernel, 2, nullptr, dimxy, nullptr, 0, nullptr, nullptr);

  if(clerror != CL_SUCCESS)
  {
    std::cout << "OpenCL: Could not enqueue the db_fft kernel! Error code:" << clerror << std::endl;
    return clerror;
  }

  OCLCheck(clFinish(commandQueue));

  // We now do inverse transform of the data
  clfftPlanHandle backPlanHandle;

  OCLCheck(clfftCreateDefaultPlan(&backPlanHandle, context, CLFFT_2D, dimxy));

  OCLCheck(clfftSetPlanPrecision(backPlanHandle, CLFFT_SINGLE));
  OCLCheck(clfftSetLayout(backPlanHandle, CLFFT_COMPLEX_PLANAR, CLFFT_COMPLEX_PLANAR));
  OCLCheck(clfftSetResultLocation(backPlanHandle, CLFFT_INPLACE));
  OCLCheck(clfftSetPlanBatchSize(backPlanHandle, n));

  OCLCheck(clfftBakePlan(backPlanHandle, 1, &commandQueue, nullptr, nullptr));

  OCLCheck(clfftEnqueueTransform(backPlanHandle, CLFFT_BACKWARD, 1, &commandQueue, 0, nullptr, nullptr, bufCt, nullptr, nullptr));

  OCLCheck(clFinish(commandQueue));

  clfftDestroyPlan(&backPlanHandle);


  cl_float* pCt = new cl_float[dimxy[0]*dimxy[1]*n];
  clerror = clEnqueueReadBuffer(commandQueue, bufCt[0], CL_TRUE, 0, sizeof(cl_float)*dimxy[0]*dimxy[1]*n, pCt, 0, NULL, NULL );

  for(int i = 0; i < n; i++)
  {
    Eigen::ArrayXXf curSlice = Eigen::Map<Eigen::ArrayXXf>(&pCt[dimxy[0]*dimxy[1]*i], dimxy[0], dimxy[1]);
    Ct.push_back(curSlice);
  }

  delete[] pCt;

  OCLCheck(clReleaseMemObject(bufCt[0]));
  OCLCheck(clReleaseMemObject(bufCt[1]));
  OCLCheck(clReleaseProgram(db_program));
  OCLCheck(clReleaseKernel(db_kernel));

  return CL_SUCCESS;
}


cl_int signal_db_one_frame(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufU[2], cl_mem bufB[2], cl_mem bufC, bool db, const float k[2], float D, float dT, const size_t dimxy[2])
{
  cl_int clerror;
  
  // We initialise the kernel we will run on the device  
  cl_program db_program;
  cl_kernel db_kernel;

  ParameterList dbParameters;
  if(db)
    dbParameters.push_back(Parameter("DB", 1));
  dbParameters.push_back(Parameter("KON", k[0]));
  dbParameters.push_back(Parameter("KOFF", k[1]));
  dbParameters.push_back(Parameter("D", D));
  dbParameters.push_back(Parameter("DT", dT));

  clerror = loadKernel("../common_cl/db_oneframe.cl", "db_oneframe", "-I ../common_cl/", context, deviceId, &db_program, &db_kernel, &dbParameters);

  if(clerror != CL_SUCCESS)
    return clerror;

  cl_mem bufCi = createEmptyBuffer(context, CL_MEM_READ_WRITE, sizeof(cl_float)*dimxy[0]*dimxy[1]);

  cl_mem bufTempC[2];
  bufTempC[0] = bufC;
  bufTempC[1] = bufCi;

  clfftPlanHandle planHandle;  
  OCLCheck(clfftCreateDefaultPlan(&planHandle, context, CLFFT_2D, dimxy));

  OCLCheck(clfftSetPlanPrecision(planHandle, CLFFT_SINGLE));
  OCLCheck(clfftSetLayout(planHandle, CLFFT_COMPLEX_PLANAR, CLFFT_COMPLEX_PLANAR));
  OCLCheck(clfftSetResultLocation(planHandle, CLFFT_INPLACE));

  OCLCheck(clfftBakePlan(planHandle, 1, &commandQueue, nullptr, nullptr));

  OCLCheck(clfftEnqueueTransform(planHandle, CLFFT_FORWARD, 1, &commandQueue, 0, nullptr, nullptr, bufU, nullptr, nullptr));
  OCLCheck(clfftEnqueueTransform(planHandle, CLFFT_FORWARD, 1, &commandQueue, 0, nullptr, nullptr, bufB, nullptr, nullptr));
  OCLCheck(clFinish(commandQueue));

  OCLCheck(clSetKernelArg(db_kernel, 0, sizeof(cl_mem), &bufU[0]));
  OCLCheck(clSetKernelArg(db_kernel, 1, sizeof(cl_mem), &bufU[1]));
  OCLCheck(clSetKernelArg(db_kernel, 2, sizeof(cl_mem), &bufB[0]));
  OCLCheck(clSetKernelArg(db_kernel, 3, sizeof(cl_mem), &bufB[1]));
  OCLCheck(clSetKernelArg(db_kernel, 4, sizeof(cl_mem), &bufTempC[0]));
  OCLCheck(clSetKernelArg(db_kernel, 5, sizeof(cl_mem), &bufTempC[1]));

  // simulation
  clerror = clEnqueueNDRangeKernel(commandQueue, db_kernel, 2, nullptr, dimxy, nullptr, 0, nullptr, nullptr);

  if(clerror != CL_SUCCESS)
  {
    std::cout << "OpenCL: Could not enqueue the kernel! Error code:" << clerror << std::endl;
    return clerror;
  }

  OCLCheck(clFinish(commandQueue));

  // We now transform C back into space domain
  OCLCheck(clfftEnqueueTransform(planHandle, CLFFT_BACKWARD, 1, &commandQueue, 0, nullptr, nullptr, bufTempC, nullptr, nullptr));
  OCLCheck(clfftEnqueueTransform(planHandle, CLFFT_BACKWARD, 1, &commandQueue, 0, nullptr, nullptr, bufU, nullptr, nullptr));
  OCLCheck(clfftEnqueueTransform(planHandle, CLFFT_BACKWARD, 1, &commandQueue, 0, nullptr, nullptr, bufB, nullptr, nullptr));
  OCLCheck(clFinish(commandQueue));

  clfftDestroyPlan(&planHandle);

  OCLCheck(clReleaseMemObject(bufCi));
  OCLCheck(clReleaseKernel(db_kernel));
  OCLCheck(clReleaseProgram(db_program));

  return CL_SUCCESS;
}


Eigen::ArrayXXf get_frame(cl_command_queue commandQueue, cl_mem bufFrame, const size_t dimxy[2])
{
  cl_float* pFrame = new cl_float[dimxy[0]*dimxy[1]];
  OCLCheck(clEnqueueReadBuffer(commandQueue, bufFrame, CL_TRUE, 0, sizeof(cl_float)*dimxy[0]*dimxy[1], pFrame, 0, NULL, NULL));

  Eigen::ArrayXXf frame = Eigen::Map<Eigen::ArrayXXf>(pFrame, dimxy[0], dimxy[1]);

  delete[] pFrame;

  return frame;
}


Eigen::ArrayXXf get_frame_fft(cl_context context, cl_command_queue commandQueue, cl_mem bufFrame[2], const size_t dimxy[2])
{
  cl_mem bufTemp[2];
  bufTemp[0] = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(cl_float)*dimxy[0]*dimxy[1], nullptr, nullptr);
  bufTemp[1] = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(cl_float)*dimxy[0]*dimxy[1], nullptr, nullptr);

  OCLCheck(clEnqueueCopyBuffer(commandQueue, bufFrame[0], bufTemp[0], 0, 0, sizeof(cl_float)*dimxy[0]*dimxy[1], 0, nullptr, nullptr));
  OCLCheck(clEnqueueCopyBuffer(commandQueue, bufFrame[1], bufTemp[1], 0, 0, sizeof(cl_float)*dimxy[0]*dimxy[1], 0, nullptr, nullptr));

  clfftPlanHandle fftHandle;
  OCLCheck(clfftCreateDefaultPlan(&fftHandle, context, CLFFT_2D, dimxy));

  OCLCheck(clfftSetPlanPrecision(fftHandle, CLFFT_SINGLE));
  OCLCheck(clfftSetLayout(fftHandle, CLFFT_COMPLEX_PLANAR, CLFFT_COMPLEX_PLANAR));
  OCLCheck(clfftSetResultLocation(fftHandle, CLFFT_INPLACE));

  OCLCheck(clfftBakePlan(fftHandle, 1, &commandQueue, nullptr, nullptr));

  OCLCheck(clfftEnqueueTransform(fftHandle, CLFFT_BACKWARD, 1, &commandQueue, 0, nullptr, nullptr, bufTemp, nullptr, nullptr));

  OCLCheck(clFinish(commandQueue));

  OCLCheck(clfftDestroyPlan(&fftHandle));
  
  cl_float* pFrame = new cl_float[dimxy[0]*dimxy[1]];
  OCLCheck(clEnqueueReadBuffer(commandQueue, bufTemp[0], CL_TRUE, 0, sizeof(cl_float)*dimxy[0]*dimxy[1], pFrame, 0, NULL, NULL));

  Eigen::ArrayXXf frame = Eigen::Map<Eigen::ArrayXXf>(pFrame, dimxy[0], dimxy[1]);

  delete[] pFrame;
  
  OCLCheck(clReleaseMemObject(bufTemp[0]));
  OCLCheck(clReleaseMemObject(bufTemp[1]));
  
  return frame;
}

