#ifndef EXPORT_SOLUTION_H
#define EXPORT_SOLUTION_H

#include<fstream>
#include<Eigen/Dense>
#include<vector>
#include<CL/opencl.h>

template<typename A>
void export_csv(const std::string& szFilename, const std::vector<A>& Ct);


template<typename A>
inline void export_csv(const std::string& szFilename, const std::vector<A>& Ct)
{
  std::ofstream outfile(szFilename);
  if(!Ct.empty())
  {
    outfile << Ct[0].rows() << "," << Ct[0].cols() << "," << Ct.size() << std::endl;
    
    for(unsigned int i = 0; i < Ct.size(); i++)
    {
      for(int j = 0; j < Ct[i].rows(); j++)
      {
	outfile << Ct[i](0, j);
	for(int k = 1; k < Ct[i].cols(); k++)
	  outfile << "," << Ct[i](k, j);

	outfile << std::endl;
      }
    }
  }
  
  outfile.close();
}

#endif // EXPORT_SOLUTION
