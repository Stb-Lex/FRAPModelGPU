#include"opencl_common.h"

#include<iostream>
#include<fstream>
#include<sstream>
#include<cstring>
#include<libconfig.h++>

std::string Parameter::ToString() const
{
  // This code is horrible but it works
  switch(m_type)
  {
  case PT_I: { return std::to_string(m_param.i); }
  case PT_F: { return std::to_string(m_param.f); }
  default: { return "ERROR"; }
  }
}


cl_int loadKernel(const std::string& szFilename, const std::string& szKernelName, const char* szBuildOptions, cl_context context, cl_device_id deviceId, cl_program* program, cl_kernel* kernel, const ParameterList* parameters, bool fastMath)
{
  cl_int clerror;
  
  std::ifstream file(szFilename);
  if(!file.is_open())
  {
    std::cout << "Could not find file " << szFilename << "!" << std::endl;
    return CL_INVALID_KERNEL;
  }
  std::stringstream t;
  t << file.rdbuf();
  char* szSource = new char[t.str().size()];
  strcpy(szSource, t.str().c_str());
  file.close();

  (*program) = clCreateProgramWithSource(context, 1, (const char**)&szSource, nullptr, &clerror);

  if(clerror != CL_SUCCESS)
  {
    std::cerr << "OpenCL: Could not create program from source! Error code: " << clerror << std::endl;
    return clerror;
  }

  // We generate the build options
  std::string szFinalBuildOptions(szBuildOptions);
  szFinalBuildOptions += " -Werror";
  
  if(fastMath)
    szFinalBuildOptions += " -cl-fast-relaxed-math";
  
  if(parameters)
    for(ParameterList::const_iterator it = parameters->begin(); it != parameters->end(); it++)
      szFinalBuildOptions += (" -D " + (*it).GetParameterName() + '=' + (*it).ToString());

  clerror = clBuildProgram((*program), 0, nullptr, szFinalBuildOptions.c_str(), nullptr, nullptr);

  if(clerror != CL_SUCCESS)
  {
    char buffer[16384];
    size_t length;
    std::cerr << "OpenCL: Could not build program " << szKernelName << " in file " << szFilename << "!" << std::endl;
    clGetProgramBuildInfo((*program), deviceId, CL_PROGRAM_BUILD_LOG, sizeof(buffer), &buffer, &length);

    std::cerr << std::string(buffer) << std::flush << std::endl;
    return clerror;
  }

  (*kernel) = clCreateKernel((*program), szKernelName.c_str(), &clerror);

  delete[] szSource;

  return CL_SUCCESS;
}

cl_int initOpenCL(cl_platform_id& platformId, cl_context& context, cl_device_id& deviceId, cl_command_queue& commandQueue, cl_device_type deviceType)
{
  const unsigned int maxPlatformId = 4;
  const unsigned int maxDevice = 4;
  
  // OpenCL related vars  
  cl_platform_id allPlatformId[maxPlatformId];
  cl_uint platformCount;

  // We initialise OpenCL
  cl_int clerror = clGetPlatformIDs(maxPlatformId, allPlatformId, &platformCount);

  if(clerror != CL_SUCCESS)
  {
    std::cerr << "OpenCL initialisation: Could not get platform IDs! Error code: " << clerror << std::endl;
    return clerror;
  }

  if(platformCount == 0)
  {
    std::cerr << "OpenCL initialisation: Could not find an OpenCL platform!" << std::endl;
    return CL_INVALID_PLATFORM;
  }

  std::cout << "Found " << platformCount << " platform(s)!" << std::endl;

  bool deviceFound = false;
  bool deviceAvailable = true;
  unsigned int i = 0;
  while(!deviceFound && deviceAvailable)
  {
    cl_device_id allDeviceId[maxDevice];
    cl_uint deviceCount;

    clerror = clGetDeviceIDs(allPlatformId[i], deviceType, maxDevice, allDeviceId, &deviceCount);

    if(clerror != CL_DEVICE_NOT_FOUND && clerror == CL_SUCCESS)
    {
      // We found at least one device with the right device type, we pick one randomly from the list
      srand(time(nullptr));
      deviceId = allDeviceId[rand() % deviceCount];
      platformId = allPlatformId[i];

      char platformName[128];
      char deviceName[128];

      clGetPlatformInfo(allPlatformId[i], CL_PLATFORM_NAME, sizeof(char)*128, platformName, nullptr);
      clGetDeviceInfo(deviceId, CL_DEVICE_NAME, sizeof(char)*128, deviceName, nullptr);

      std::cout << "Platform found: " << platformName << std::endl;
      std::cout << "Device found: " << deviceName << std::endl;

      deviceFound = true;
    }
    else
    {
      if(clerror != CL_SUCCESS && clerror != CL_DEVICE_NOT_FOUND)
      {
	std::cerr << "OpenCL: Could not get device IDs! Error code: " << clerror << std::endl;
	return clerror;
      }

      i++;
      
      if(i >= platformCount)
	deviceAvailable = false;
    }
  }

  if(!deviceFound)
  {
    std::cerr << "OpenCL initialisation: Could not find suitable device!" << std::endl;
    return CL_DEVICE_NOT_FOUND;
  }

  // We found a device
  cl_context_properties properties[3];
  properties[0] = CL_CONTEXT_PLATFORM;
  properties[1] = (cl_context_properties)platformId;
  properties[2] = 0;

  context = clCreateContext(properties, 1, &deviceId, nullptr, nullptr, &clerror);
  
  if(clerror != CL_SUCCESS)
  {
    std::cerr << "OpenCL: Could not create context! Error code: " << clerror << std::endl;
    return -1;
  }

  commandQueue = clCreateCommandQueue(context, deviceId, 0, &clerror);

  if(clerror != CL_SUCCESS)
  {
    std::cerr << "OpenCL: Could not create command queue! Error code: " << clerror << std::endl;
    return -1;
  }

  return CL_SUCCESS;
}


cl_int initOpenCLByPlatformName(cl_platform_id& platformId, cl_context& context, cl_device_id& deviceId, cl_command_queue& commandQueue, const char* szPlatformName, cl_device_type deviceType, int deviceIndex)
{
  const unsigned int maxPlatformId = 4;
  const unsigned int maxDevice = 4;
  
  // OpenCL related vars  
  cl_platform_id allPlatformId[maxPlatformId];
  cl_uint platformCount;

  // We initialise OpenCL
  cl_int clerror = clGetPlatformIDs(maxPlatformId, allPlatformId, &platformCount);

  if(clerror != CL_SUCCESS)
  {
    std::cerr << "OpenCL initialisation: Could not get platform IDs! Error code: " << clerror << std::endl;
    return clerror;
  }

  if(platformCount == 0)
  {
    std::cerr << "OpenCL initialisation: Could not find an OpenCL platform!" << std::endl;
    return CL_INVALID_PLATFORM;
  }

  std::cout << "Found " << platformCount << " platform(s)!" << std::endl;

  bool deviceFound = false;
  bool deviceAvailable = true;
  unsigned int i = 0;
  while(!deviceFound && deviceAvailable)
  {
    cl_device_id allDeviceId[maxDevice];
    cl_uint deviceCount;

    char curPlatformName[128];
    
    clGetPlatformInfo(allPlatformId[i], CL_PLATFORM_NAME, sizeof(char)*128, curPlatformName, nullptr);

    if(strcmp(curPlatformName, szPlatformName) != 0)
    {
      // It is not the right platform
      i++;
      if(i < platformCount)
	continue;
      else
	deviceAvailable = false;
    }

    clerror = clGetDeviceIDs(allPlatformId[i], deviceType, maxDevice, allDeviceId, &deviceCount);

    if(clerror != CL_DEVICE_NOT_FOUND && clerror == CL_SUCCESS)
    {
      // We found at least one device with the right device type, we pick one randomly from the list
      srand(time(nullptr));
      if(deviceIndex == -1)
	deviceId = allDeviceId[rand() % deviceCount];
      else
	deviceId = allDeviceId[deviceIndex];      
      platformId = allPlatformId[i];

      char deviceName[128];

      clGetDeviceInfo(deviceId, CL_DEVICE_NAME, sizeof(char)*128, deviceName, nullptr);

      std::cout << "Platform found: " << curPlatformName << std::endl;
      std::cout << "Device found: " << deviceName << std::endl;

      deviceFound = true;
    }
    else
    {
      if(clerror != CL_SUCCESS && clerror != CL_DEVICE_NOT_FOUND)
      {
	std::cerr << "OpenCL: Could not get device IDs! Error code: " << clerror << std::endl;
	return clerror;
      }

      i++;
      
      if(i >= platformCount)
	deviceAvailable = false;
    }
  }

  if(!deviceFound)
  {
    std::cerr << "OpenCL initialisation: Could not find suitable device!" << std::endl;
    return CL_DEVICE_NOT_FOUND;
  }

  // We found a device
  cl_context_properties properties[3];
  properties[0] = CL_CONTEXT_PLATFORM;
  properties[1] = (cl_context_properties)platformId;
  properties[2] = 0;

  context = clCreateContext(properties, 1, &deviceId, nullptr, nullptr, &clerror);
  
  if(clerror != CL_SUCCESS)
  {
    std::cerr << "OpenCL: Could not create context! Error code: " << clerror << std::endl;
    return -1;
  }

  commandQueue = clCreateCommandQueue(context, deviceId, 0, &clerror);

  if(clerror != CL_SUCCESS)
  {
    std::cerr << "OpenCL: Could not create command queue! Error code: " << clerror << std::endl;
    return -1;
  }

  return CL_SUCCESS;
}


cl_int initOpenCLByConfigFile(cl_platform_id& platform, cl_context& context, cl_device_id& deviceId, cl_command_queue& commandQueue, const char* configFilename)
{
  libconfig::Config deviceConfig;
  
  try
  {
    deviceConfig.readFile(configFilename);
  }
  catch(libconfig::ParseException& e)
  {
    std::cout << "Error when parsing device config file " << configFilename << " at line " << e.getLine() << std::endl;
    std::cout << e.getError() << std::endl;
    return -1;
  }
  catch(libconfig::FileIOException& e)
  {
    std::cout << "Could not open device config file " << configFilename << std::endl;
    return -1;
  }

  std::string platformName;
  std::string platformType;
  int deviceIndex = -1;

  try
  {
    deviceConfig.lookup("device.platformName");
    deviceConfig.lookup("device.deviceType");
  }
  catch(libconfig::SettingNotFoundException& e)
  {
    std::cout << "Setting not found! " << e.getPath() << std::endl;
    return -1;
  }

  deviceConfig.lookupValue("device.platformName", platformName);
  deviceConfig.lookupValue("device.deviceType", platformType);
  deviceConfig.lookupValue("device.deviceIndex", deviceIndex);

  cl_device_type type;
  if(platformType == "CPU")
    type = CL_DEVICE_TYPE_CPU;
  else if(platformType == "GPU")
    type = CL_DEVICE_TYPE_GPU;
  else
  {
    std::cout << "Could not understand platform type " << platformType << "!" << std::endl;
    return -1;
  }
  
  return initOpenCLByPlatformName(platform, context, deviceId, commandQueue, platformName.c_str(), type, deviceIndex);
}


cl_mem createEmptyBuffer(cl_context context, cl_mem_flags flags, size_t size, cl_int* errcode_ret)
{
  cl_uchar* pEmptyBuffer = new cl_uchar[size];
  memset(pEmptyBuffer, 0x00, size);
  cl_mem buf = clCreateBuffer(context, flags | CL_MEM_COPY_HOST_PTR, size, pEmptyBuffer, errcode_ret);
  delete[] pEmptyBuffer;
  return buf;
}

