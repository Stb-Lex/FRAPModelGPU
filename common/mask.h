#ifndef MASK_H
#define MASK_H

#include<CL/opencl.h>

cl_int create_mask_disk(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem& bufMask, const size_t dimxy[2], float radius, float Ib, float Iu, unsigned int mult = 1);
cl_int create_mask_rect(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem& bufMask, const size_t dimxy[2], const float rectDim[2], float Ib, float Iu, unsigned int mult = 1);
cl_int create_mask_gaussian(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem& bufMask, const size_t dimxy[2], float radius, float Ib, float Iu);

cl_int downsample(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufInitial, cl_mem bufDownsampled, const size_t dimxy[2], unsigned int mult);

#endif // MASK_H
