#include"particles_common.h"
#include"opencl_common.h"

#include<iostream>
#include<vector>
#include<cmath>

cl_int zoom(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufPos, size_t workers, unsigned int particlesPerWorker, size_t dimxy[2], float zoomFactor)
{
  cl_int clerror;
  cl_program zoom_program;
  cl_kernel zoom_kernel;

  ParameterList parameters;
  parameters.push_back(Parameter("DIMX", (int)dimxy[0]));
  parameters.push_back(Parameter("DIMY", (int)dimxy[1]));
  parameters.push_back(Parameter("PARTICLESPERWORKER", (int)particlesPerWorker));
  parameters.push_back(Parameter("ZOOM", zoomFactor));
  
  clerror = loadKernel("../common_cl/stochastic_zoom.cl", "to_zoomed_space", "", context, deviceId, &zoom_program, &zoom_kernel, &parameters);

  if(clerror != CL_SUCCESS)
    return clerror;

  OCLCheck(clSetKernelArg(zoom_kernel, 0, sizeof(cl_mem), &bufPos));

  clerror = clEnqueueNDRangeKernel(commandQueue, zoom_kernel, 1, nullptr, &workers, nullptr, 0, nullptr, nullptr);

  if(clerror != CL_SUCCESS)
  {
    std::cerr << "Error during zooming! OpenCL error code: " << clerror;
    return clerror;
  }

  OCLCheck(clReleaseKernel(zoom_kernel));
  OCLCheck(clReleaseProgram(zoom_program));

  return CL_SUCCESS;  
}


cl_int unzoom(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufPos, size_t workers, unsigned int particlesPerWorker, size_t dimxy[2], float zoomFactor)
{
  cl_int clerror;
  cl_program zoom_program;
  cl_kernel zoom_kernel;

  ParameterList parameters;
  parameters.push_back(Parameter("DIMX", (int)dimxy[0]));
  parameters.push_back(Parameter("DIMY", (int)dimxy[1]));
  parameters.push_back(Parameter("PARTICLESPERWORKER", (int)particlesPerWorker));
  parameters.push_back(Parameter("ZOOM", zoomFactor));
  
  clerror = loadKernel("../common_cl/stochastic_zoom.cl", "to_unzoomed_space", "", context, deviceId, &zoom_program, &zoom_kernel, &parameters);

  if(clerror != CL_SUCCESS)
    return clerror;

  OCLCheck(clSetKernelArg(zoom_kernel, 0, sizeof(cl_mem), &bufPos));

  clerror = clEnqueueNDRangeKernel(commandQueue, zoom_kernel, 1, nullptr, &workers, nullptr, 0, nullptr, nullptr);

  if(clerror != CL_SUCCESS)
  {
    std::cerr << "Error during unzooming! OpenCL error code: " << clerror;
    return clerror;
  }

  OCLCheck(clReleaseKernel(zoom_kernel));
  OCLCheck(clReleaseProgram(zoom_program));

  return CL_SUCCESS;
}


#define STATE_INACTIVE (1 << 1)

unsigned int consolidate_active_particles(cl_command_queue commandQueue, cl_mem bufPos, cl_mem bufState, unsigned int workers, unsigned int particlesPerWorker)
{
  std::vector<cl_float2> newPos;
  std::vector<cl_uchar> newState;

  cl_float2* pPos = new cl_float2[workers*particlesPerWorker];
  cl_uchar* pState = new cl_uchar[workers*particlesPerWorker];

  OCLCheck(clEnqueueReadBuffer(commandQueue, bufPos, CL_TRUE, 0, sizeof(cl_float2)*workers*particlesPerWorker, pPos, 0, nullptr, nullptr));
  OCLCheck(clEnqueueReadBuffer(commandQueue, bufState, CL_TRUE, 0, sizeof(cl_uchar)*workers*particlesPerWorker, pState, 0, nullptr, nullptr));

  unsigned int inactive = 0;
  for(unsigned int i = 0; i < workers*particlesPerWorker; i++)
  {
    if((bool)(pState[i] & STATE_INACTIVE))
    {
      inactive++;
      continue;
    }
    
    newPos.push_back(pPos[i]);
    newState.push_back(pState[i]);
  }

  unsigned int newParticlesPerWorker = floor((float)newPos.size()/workers);

  OCLCheck(clEnqueueWriteBuffer(commandQueue, bufPos, CL_TRUE, 0, sizeof(cl_float2)*workers*newParticlesPerWorker, newPos.data(), 0, nullptr, nullptr));
  OCLCheck(clEnqueueWriteBuffer(commandQueue, bufState, CL_TRUE, 0, sizeof(cl_uchar)*workers*newParticlesPerWorker, newState.data(), 0, nullptr, nullptr));

  return newParticlesPerWorker;
}
