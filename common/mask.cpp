#include"mask.h"
#include"opencl_common.h"
#include<iostream>


cl_int create_mask_disk(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem& bufMask, const size_t dimxy[2], float radius, float Ib, float Iu, unsigned int mult)
{
  if(mult == 0)
    mult = 1;
  
  // We first generate a scaled, rough, version of the initial conditions and then down sample it in order to smooth discontinuities
  cl_int clerror;
  bufMask = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(cl_float)*dimxy[0]*dimxy[1], nullptr, nullptr);
  cl_mem bufMaskScaled = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(cl_float)*mult*mult*dimxy[0]*dimxy[1], nullptr, nullptr);

  cl_kernel mask_kernel;
  cl_kernel downsample_kernel;
  cl_program mask_program;
  cl_program downsample_program;

  ParameterList parameterListMask;
  parameterListMask.push_back(Parameter("RADIUS", (float)mult*radius));
  parameterListMask.push_back(Parameter("IU", Iu));
  parameterListMask.push_back(Parameter("IB", Ib));
  
  clerror = loadKernel("../common_cl/init_disk.cl", "init_disk", "-I ../common_cl/", context, deviceId, &mask_program, &mask_kernel, &parameterListMask);

  if(clerror != CL_SUCCESS)
    return clerror;

  OCLCheck(clSetKernelArg(mask_kernel, 0, sizeof(cl_mem), &bufMaskScaled));

  // We first generate a upscaled version of the initial conditions
  size_t sizeKernelScaled[2] = {mult*dimxy[0], mult*dimxy[1]};
  clerror = clEnqueueNDRangeKernel(commandQueue, mask_kernel, 2, nullptr, sizeKernelScaled, nullptr, 0, nullptr, nullptr);

  if(clerror != CL_SUCCESS)
  {
    std::cout << "Could not generate upsampled mask! OpenCL error: " << clerror << std::endl;
    return clerror;
  }

  if(mult > 1)
  {
    clerror = downsample(context, commandQueue, deviceId, bufMaskScaled, bufMask, dimxy, mult);

    if(clerror != CL_SUCCESS)
      return clerror;

    OCLCheck(clReleaseMemObject(bufMaskScaled));
  }
  else
  {
    OCLCheck(clReleaseMemObject(bufMask));
    bufMask = bufMaskScaled;
  }

  OCLCheck(clReleaseKernel(mask_kernel));
  OCLCheck(clReleaseProgram(mask_program));

  return CL_SUCCESS;
}


cl_int create_mask_rect(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem& bufMask, const size_t dimxy[2], const float rectDim[2], float Ib, float Iu, unsigned int mult)
{
  if(mult == 0)
    mult = 1;
  
  // We first generate a scaled, rough, version of the initial conditions and then down sample it in order to smooth discontinuities
  cl_int clerror;
  bufMask = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(cl_float)*dimxy[0]*dimxy[1], nullptr, nullptr);
  cl_mem bufMaskScaled = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(cl_float)*mult*mult*dimxy[0]*dimxy[1], nullptr, nullptr);

  cl_kernel mask_kernel;
  cl_program mask_program;

  ParameterList parameterListMask;
  parameterListMask.push_back(Parameter("RECTDIMX", (float)mult*rectDim[0]));
  parameterListMask.push_back(Parameter("RECTDIMY", (float)mult*rectDim[1]));
  parameterListMask.push_back(Parameter("IU", Iu));
  parameterListMask.push_back(Parameter("IB", Ib));
  
  clerror = loadKernel("../common_cl/init_rect.cl", "init_rect", "-I ../common_cl/", context, deviceId, &mask_program, &mask_kernel, &parameterListMask);

  if(clerror != CL_SUCCESS)
    return clerror;

  OCLCheck(clSetKernelArg(mask_kernel, 0, sizeof(cl_mem), &bufMaskScaled));

  // We first generate a upscaled version of the initial conditions
  size_t sizeKernelScaled[2] = {mult*dimxy[0], mult*dimxy[1]};
  clerror = clEnqueueNDRangeKernel(commandQueue, mask_kernel, 2, nullptr, sizeKernelScaled, nullptr, 0, nullptr, nullptr);

  if(clerror != CL_SUCCESS)
  {
    std::cout << "Could not generate upsampled mask! OpenCL error: " << clerror << std::endl;
    return clerror;
  }
  if(mult > 1)
  {
    clerror = downsample(context, commandQueue, deviceId, bufMaskScaled, bufMask, dimxy, mult);

    if(clerror != CL_SUCCESS)
      return clerror;

    OCLCheck(clReleaseMemObject(bufMaskScaled));
  }
  else
  {
    OCLCheck(clReleaseMemObject(bufMask));
    bufMask = bufMaskScaled;
  }

  OCLCheck(clReleaseKernel(mask_kernel));
  OCLCheck(clReleaseProgram(mask_program));

  return CL_SUCCESS;
}

cl_int create_mask_gaussian(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem& bufMask, const size_t dimxy[2], float radius, float Ib, float Iu)
{
  cl_kernel mask_kernel;
  cl_program mask_program;

  bufMask = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(cl_float)*dimxy[0]*dimxy[1], nullptr, nullptr);

  ParameterList parameterListMask;
  parameterListMask.push_back(Parameter("RADIUS", radius));
  parameterListMask.push_back(Parameter("IU", Iu));
  parameterListMask.push_back(Parameter("IB", Ib));

  cl_int clerror = loadKernel("../common_cl/init_gaussian.cl", "init_gaussian", "", context, deviceId, &mask_program, &mask_kernel, &parameterListMask);

  if(clerror != CL_SUCCESS)
    return clerror;

  OCLCheck(clSetKernelArg(mask_kernel, 0, sizeof(cl_mem), &bufMask));

  clerror = clEnqueueNDRangeKernel(commandQueue, mask_kernel, 2, nullptr, dimxy, nullptr, 0, nullptr, nullptr);

  if(clerror != CL_SUCCESS)
  {
    std::cout << "Initial condition: Could not generate gaussian initial condition! OpenCL error: " << clerror << std::endl;
    return clerror;
  }

  OCLCheck(clFinish(commandQueue));
  
  OCLCheck(clReleaseKernel(mask_kernel));
  OCLCheck(clReleaseProgram(mask_program));

  return CL_SUCCESS;
}

cl_int downsample(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufInitial, cl_mem bufDownsampled, const size_t dimxy[2], unsigned int mult)
{
  cl_kernel downsample_kernel;
  cl_program downsample_program;

  ParameterList parameterListDownsample;
  parameterListDownsample.push_back(Parameter("KERNEL_SIZE", (int)mult));

  cl_int clerror = loadKernel("../common_cl/downsample.cl", "downsample", "-I ../common_cl/", context, deviceId, &downsample_program, &downsample_kernel, &parameterListDownsample);

  if(clerror != CL_SUCCESS)
    return clerror;

  OCLCheck(clSetKernelArg(downsample_kernel, 0, sizeof(cl_mem), &bufInitial));
  OCLCheck(clSetKernelArg(downsample_kernel, 1, sizeof(cl_mem), &bufDownsampled));

  clerror = clEnqueueNDRangeKernel(commandQueue, downsample_kernel, 2, nullptr, dimxy, nullptr, 0, nullptr, nullptr);

  if(clerror != CL_SUCCESS)
  {
    std::cout << "Initial condition: Could not downsample! OpenCL error: " << clerror << std::endl;
    return clerror;
  }

  OCLCheck(clReleaseKernel(downsample_kernel));
  OCLCheck(clReleaseProgram(downsample_program));

  return CL_SUCCESS;
}
