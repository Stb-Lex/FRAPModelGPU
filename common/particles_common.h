#ifndef PARTICLES_COMMON_H
#define PARTICLES_COMMON_H

#include<CL/opencl.h>

cl_int zoom(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufPos, size_t workers, unsigned int particlesPerWorker, size_t dimxy[2], float zoomFactor);

cl_int unzoom(cl_context context, cl_command_queue commandQueue, cl_device_id deviceId, cl_mem bufPos, size_t workers, unsigned int particlesPerWorker, size_t dimxy[2], float zoomFactor);

// Remove particles that are inactive (i.e. bleached)
// Returns the new number of particlesPerWorker
unsigned int consolidate_active_particles(cl_command_queue commandQueue, cl_mem bufPos, cl_mem bufState, unsigned int Nworkers, unsigned int particlesPerWorker);


#endif // PARTICLES_COMMON_H
