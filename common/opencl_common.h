#ifndef OPENCL_COMMON_H
#define OPENCL_COMMON_H

#include<CL/opencl.h>

#include<iostream>
#include<string>
#include<list>

#define OCLCheck(fct) { cl_int a = fct; if(a != CL_SUCCESS){std::cout << "OpenCL Error " << a << " when calling: " << #fct << " at line " << __LINE__ << " in file " << __FILE__ << std::endl;}}

union ParameterValue
{
  ParameterValue() { i = 0; }
  ParameterValue(int _i) { i = _i; }
  ParameterValue(float _f) { f = _f; }

  ParameterValue& operator=(const ParameterValue& o) { i = o.i; return *this; }
  
  int i;
  float f;
};

enum ParameterType
{
  PT_UNKNOWN,
  PT_I,
  PT_F
};

class Parameter
{
 public:
  Parameter() { m_type = PT_UNKNOWN; }
  Parameter(const std::string szName, int _i) { m_param.i = _i; m_type = PT_I; m_paramName = szName; }
  Parameter(const std::string szName, float _f) { m_param.f = _f; m_type = PT_F; m_paramName = szName; }

  std::string ToString() const;

  std::string GetParameterName() const { return m_paramName; }

 private:
  ParameterType m_type;
  ParameterValue m_param;
  std::string m_paramName;
};

typedef std::list<Parameter> ParameterList;

cl_int loadKernel(const std::string& szFilename, const std::string& szKernelName, const char* szBuildOptions, cl_context context, cl_device_id deviceId, cl_program* program, cl_kernel* kernel, const ParameterList* parameters, bool fastMath = true);

cl_int initOpenCL(cl_platform_id& platform, cl_context& context, cl_device_id& deviceId, cl_command_queue& commandQueue, cl_device_type deviceType);
cl_int initOpenCLByPlatformName(cl_platform_id& platform, cl_context& context, cl_device_id& deviceId, cl_command_queue& commandQueue, const char* szPlatformName, cl_device_type deviceType, int deviceIndex = -1);
cl_int initOpenCLByConfigFile(cl_platform_id& platform, cl_context& context, cl_device_id& deviceId, cl_command_queue& commandQueue, const char* configFilename);

cl_mem createEmptyBuffer(cl_context context, cl_mem_flags flags, size_t size, cl_int* errcode_ret = nullptr);

#endif // OPENCL_COMMON_H
