#ifndef TIMER_H
#define TIMER_H

#include<chrono>

class Timer
{
 public:
  Timer(void)
  {
    m_initTime = std::chrono::high_resolution_clock::now();
  }

  double Elapsed(void) const
  {
    std::chrono::high_resolution_clock::time_point curTime = std::chrono::high_resolution_clock::now();
    return std::chrono::duration_cast<std::chrono::duration<double>>(curTime - m_initTime).count();
  }

 private:
  std::chrono::high_resolution_clock::time_point m_initTime;

};

#endif // TIMER_H
